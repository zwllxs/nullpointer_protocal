INSERT INTO "np_attribute_kv"("entity_type", "entity_id", "attribute_type", "attribute_key",
                              "bool_v", "str_v", "long_v", "dbl_v", "last_update_ts")
VALUES ('DEVICE', '1ea2154c0424740b41dcdc50ab453cd',
        'SERVER_SCOPE', 'initial_value', NULL, null, null, 0, floor(extract(epoch from((current_timestamp
- timestamp '1970-01-01 00:00:00')*1000))));

INSERT INTO "np_attribute_kv"("entity_type", "entity_id",
                              "attribute_type", "attribute_key", "bool_v", "str_v", "long_v", "dbl_v", "last_update_ts")
VALUES ('DEVICE', '1ea2154c0424740b41dcdc50ab453cd', 'SERVER_SCOPE', 'depth', NULL, null, null,
        0, floor(extract(epoch from((current_timestamp - timestamp '1970-01-01 00:00:00')*1000))));
INSERT INTO "np_attribute_kv"("entity_type", "entity_id", "attribute_type", "attribute_key",
                              "bool_v", "str_v", "long_v", "dbl_v", "last_update_ts")
VALUES ('DEVICE', '1ea2154c0424740b41dcdc50ab453cd',
        'SERVER_SCOPE', 'control_flag', NULL, null, null, 0, floor(extract(epoch from((current_timestamp
- timestamp '1970-01-01 00:00:00')*1000))));
INSERT INTO "np_attribute_kv"("entity_type", "entity_id",
                              "attribute_type", "attribute_key", "bool_v", "str_v", "long_v", "dbl_v", "last_update_ts")
VALUES ('DEVICE', '1ea2154c0424740b41dcdc50ab453cd', 'SERVER_SCOPE', 'height', NULL, null, null,
        0, floor(extract(epoch from((current_timestamp - timestamp '1970-01-01 00:00:00')*1000))));
INSERT INTO "np_attribute_kv"("entity_type", "entity_id", "attribute_type", "attribute_key",
                              "bool_v", "str_v", "long_v", "dbl_v", "last_update_ts")
VALUES ('DEVICE', '1ea2154c0424740b41dcdc50ab453cd',
        'SERVER_SCOPE', 'ratio', NULL, null, null, 0, floor(extract(epoch from((current_timestamp -
timestamp '1970-01-01 00:00:00')*1000))));;