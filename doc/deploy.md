### 一：前提准备
    1:基于spring boot 1.5.15，数据库为postgresql,
    2:依赖 第一步开发的包，git地址:  https://git.coding.net/zwllxs/boot-core.git
    可以将其下载到本地并install到本地，或者deploy到自己的构建私有仓库中。
    执行命令如下:
        mvn install 或 mvn deploy
    ps:如果需要deploy，需要配置自己的私有仓库，本文档略去此过程
    
### 二: 部署
    1: 下载源码,git地址: https://gitee.com/zwllxs/nullpointer_protocal.git
    2: 进入目录，直接打包:
        mvn package
      可得到一可执行jar包: front-end-1.0-SNAPSHOT-exec.jar
    3: 执行java -jar front-end-1.0-SNAPSHOT-exec.jar可运行
    4: 第三步为默认的开发环境，如果想指定为生产环境，可执行如下:
        nohup java -jar  -Dspring.profiles.active=prod  front-end-1.0-SNAPSHOT-exec.jar  2>&1 &
    
###  三: 结构简介
    工程名为nullpointer_protocal,表示为物联网协议层，它结构如下:
    front-end: 设备接入层工程，也为启动的模块，与设备直接打交道，可集成mqtt接入，tcp接入.
    tcp-server: tcp服务器端，做为front-end的依赖，启动front-end模块，会调用此开启tcp服务端。同时它内置了服务端必要的各种编码解码器，如用于在服务端对报文加解密等。
    tcp-core：tcp的各种操作，既可用于做tcp-server的依赖，也可用于别的tcp模块依赖，如再来个tcp-client什么的。它内置了对协议报文的操作编解码器和对具体数据的各种转换器，且可自由配置和拓展。
    
###  四: 配置数据源
    在front-end工程下，有如下四个配置文件:
        application.properties
        application-dev.properties
        application-sit.properties
        application-prod.properties
    它们对应三个不同环境，线上运行时，可参考第二步给到的命令。