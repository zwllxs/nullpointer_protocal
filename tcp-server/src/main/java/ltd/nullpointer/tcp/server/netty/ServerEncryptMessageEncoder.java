package ltd.nullpointer.tcp.server.netty;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import ltd.nullpointer.tcp.core.EncryptMessageEncoder;
import ltd.nullpointer.tcp.core.SessionManager;
import org.springframework.stereotype.Component;

/**
 * 
 * @ClassName: ServerEncryptMessageEncoder.java
 * @Description:  加密
 * @author zhangweilin
 * @date 2018年1月22日 下午7:55:24
 *
 */
@Component
@ChannelHandler.Sharable
public class ServerEncryptMessageEncoder extends EncryptMessageEncoder {

	private SessionManager sessionManager;
	
	public ServerEncryptMessageEncoder(SessionManager sessionManager) {
		super();
		this.sessionManager = sessionManager;
	}

	@Override
	protected String getAesKey(ChannelHandlerContext ctx) {
		String aesKey=sessionManager.getAesKey(ctx.channel());
//		System.out.println("ServerEncryptMessageEncoder.aesKey: "+aesKey);
		return aesKey;
	}
}
