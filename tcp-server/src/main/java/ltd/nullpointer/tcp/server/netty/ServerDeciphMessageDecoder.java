package ltd.nullpointer.tcp.server.netty;

import io.netty.channel.ChannelHandlerContext;
import ltd.nullpointer.tcp.core.DeciphMessageDecoder;
import ltd.nullpointer.tcp.core.SessionManager;
import org.springframework.stereotype.Component;

/**
 * 
 * @ClassName: ServerDeciphMessageDecoder.java
 * @Description:  解密
 * @author zhangweilin
 * @date 2018年1月22日 下午7:54:48
 *
 */
@Component
public class ServerDeciphMessageDecoder extends DeciphMessageDecoder {

	private SessionManager sessionManager;
	
	public ServerDeciphMessageDecoder(SessionManager sessionManager) {
		super();
		this.sessionManager = sessionManager;
	}

	@Override
	protected String getAesKey(ChannelHandlerContext ctx) {
		String aesKey=sessionManager.getAesKey(ctx.channel()); 
//		System.out.println("ServerDeciphMessageDecoder.aesKey: "+aesKey);
		return aesKey;
	}
}
