package ltd.nullpointer.tcp.server.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import lombok.Data;

/**
* @author zhangweilin
* @date 2019/12/5 10:44
* @Description:
*/
//@Component
@Data
public class TCPServer extends Thread {

//	@Autowired
//	@Qualifier("serverBootstrap")
	private ServerBootstrap serverBootstrap;

//	@Autowired
//	@Qualifier("tcpSocketAddress")
//	private InetSocketAddress tcpPort;


	private int tcpPort;

	private Channel serverChannel;

	/**
	 * Allocates a new {@code Thread} object. This constructor has the same
	 * effect as {@linkplain #Thread(ThreadGroup, Runnable, String) Thread}
	 * {@code (null, null, gname)}, where {@code gname} is a newly generated
	 * name. Automatically generated names are of the form
	 * {@code "Thread-"+}<i>n</i>, where <i>n</i> is an integer.
	 */
	public TCPServer(ServerBootstrap serverBootstrap, int tcpPort) {
		this.serverBootstrap = serverBootstrap;
		this.tcpPort = tcpPort;
	}

	@Override
	public void run()  {
		try {
			System.out.println("TCP服务已开启,监听端口为: "+tcpPort);
			serverChannel = serverBootstrap.bind(tcpPort).sync().channel().closeFuture().sync().channel();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

//	@PreDestroy
//	public void stop() {
//		if (serverChannel != null) {
//			serverChannel.close();
//			serverChannel.parent().close();
//		}
//	}


}
