package ltd.nullpointer.tcp.server.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import ltd.nullpointer.tcp.core.AbstractHlChannelHandler;
import ltd.nullpointer.tcp.core.SessionManager;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractChannelDuplexHandler extends ChannelDuplexHandler implements AbstractHlChannelHandler {
	protected Log log = LogFactory.getLog(this.getClass());

	@Autowired
	protected SessionManager sessionManager;

//	@Autowired
//	protected KafkaProducer<Object> kafkaProducer;

//	@Autowired
//	protected ProductJpaDao productJpaDao;

	@Override
	public Channel getChannelHandlerContext(AbstractTCPMessage abstractTCPMessage) {
		return sessionManager.getChannelByDeviceId(abstractTCPMessage.getDeviceSn());
	}

}
