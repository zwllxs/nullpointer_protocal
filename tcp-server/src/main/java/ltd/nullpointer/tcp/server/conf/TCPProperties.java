package ltd.nullpointer.tcp.server.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/5
 */
@Component
@Configuration
@ConfigurationProperties("tcp")
@Data
public class TCPProperties {

    /**
     * 支持多端口，以逗号隔开
     */
    private Set<Integer> port;

    private int bossThreadCount;

    private int workerThreadCount;

    private boolean soKeepalive;

    private int soBacklog;
}
