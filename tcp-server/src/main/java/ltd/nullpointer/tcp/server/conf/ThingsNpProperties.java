package ltd.nullpointer.tcp.server.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author zhangweilin
 * @Description: ThingsNp 配置
 * @date 2019/12/5
 */
@Component
@Configuration
@ConfigurationProperties("thingsNp")
@Data
public class ThingsNpProperties {

    /**
     * 数据发往thingsNp的根路径, http接口,默认情况为http://localhost:8080/api/v1/
     */
    private String baseHttpUrl = "http://localhost:8080/api/v1/";


}
