package ltd.nullpointer.tcp.core.message;

/**
 * 外接协议桥接
 * 
 * @Description:
 * @CreateDate:2018年4月21日 下午5:23:40
 * @Author:zhangweilin
 */
public  class TcpMessageAdaptor<T> extends AbstractTCPMessage<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2825820200229146843L;

	public TcpMessageAdaptor() {
	}

	public TcpMessageAdaptor(T t) {
		setPayload(t);
	}

	//	/**
//	 * 消息类型
//	 */
//	private String msgType;
//
//
//
//	public String getMsgType() {
//		return msgType;
//	}
//
//	public void setMsgType(String msgType) {
//		this.msgType = msgType;
//	}
}
