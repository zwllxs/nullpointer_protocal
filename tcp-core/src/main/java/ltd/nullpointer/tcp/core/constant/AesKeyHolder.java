package ltd.nullpointer.tcp.core.constant;

/**
 * 
 * @ClassName: AesKeyHoler.java
 * @Description:
 * @author zhangweilin
 * @date 2018年1月22日 下午5:45:31
 *
 */
public class AesKeyHolder {
	private static transient volatile int aesKeyCount = 0;
	private static String aesKey;

	/**
	 * 设置密钥
	 * 
	 * @param aesKey0
	 */
	public synchronized static void setAesKey(String aesKey0) {
		if (++aesKeyCount > 1) {
			throw new IllegalArgumentException("this method can be only called one time");
		}
		aesKey = aesKey0;
	}

	public static String getAesKey() {
		return aesKey;
	}
}
