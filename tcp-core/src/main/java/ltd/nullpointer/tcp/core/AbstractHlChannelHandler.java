package ltd.nullpointer.tcp.core;

import io.netty.channel.Channel;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;
import ltd.nullpointer.tcp.core.message.NPiotTCPMessage;
import ltd.nullpointer.tcp.core.util.ChannelHandlerContextUtil;

/**
 * 
 * @ClassName: AbstractHlChannelHandler.java
 * @author zhangweilin
 * @date 2018年1月25日 下午3:01:37
 *
 */
public interface AbstractHlChannelHandler {
	public static SyncFuture<NPiotTCPMessage> syncFuture = new SyncFuture<>();

	default void send(NPiotTCPMessage npiotTCPMessage) {
		getChannelHandlerContext(npiotTCPMessage).writeAndFlush(npiotTCPMessage);
	}

	/**
	 * 同步发送，直接返回结果
	 * 
	 * @param abstractMessage
	 * @return
	 */
	default NPiotTCPMessage sendSync(NPiotTCPMessage npiotTCPMessage) {
		try {
			syncFuture.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ChannelHandlerContextUtil.sendSync(getChannelHandlerContext(npiotTCPMessage), npiotTCPMessage);
	}

	Channel getChannelHandlerContext(AbstractTCPMessage abstractTCPMessage);
}
