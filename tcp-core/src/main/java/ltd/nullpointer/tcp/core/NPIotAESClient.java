package ltd.nullpointer.tcp.core;

/**
 * 
 * @ClassName: NPIotRSAClient.java
 * @Description:  rsa client
 * @author zhangweilin
 * @date 2018年1月22日 下午4:57:38
 *
 */
public class NPIotAESClient extends NPIotClient {

	private String publicKey; 
	private String privateKey; 
	
	public NPIotAESClient(String ip, int port,String publicKey, String privateKey) {
		super(ip, port);
		this.publicKey=publicKey;
		this.privateKey=privateKey;
	}

	@Override
	public synchronized void setListner(AbstractClientListener abstractClientListener) {
		abstractClientListener.setEncryType(EncryType.AES);
		abstractClientListener.setPrivateKey(privateKey);
		abstractClientListener.setPublicKey(publicKey);
		this.abstractClientListener=abstractClientListener;
	}
}
