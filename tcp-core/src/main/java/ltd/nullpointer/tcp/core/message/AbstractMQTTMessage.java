package ltd.nullpointer.tcp.core.message;

/**
 * @ClassName: AbstractMQTTMessage.java
 * @Description: mqtt message
 * @author zhangweilin
 * @date 2018年1月25日 下午3:53:19
 *
 */
public class AbstractMQTTMessage<T> extends BaseMessage<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1222910264483466231L;

	/**
	 * 消息序号
	 */
	private Integer msgSn = 0;

	private String deviceSn;

	/**
	 * 版本号
	 */
	private String v = "1.0";

	/**
	 * 时间戳
	 */
	private Long t;

	public Integer getMsgSn() {
		return msgSn;
	}

	public void setMsgSn(Integer msgSn) {
		this.msgSn = msgSn;
	}

	public String getV() {
		return v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public Long getT() {
		return System.currentTimeMillis();
	}

	public void setT(Long t) {
		this.t = t;
	}

	public String getDeviceSn() {
		return deviceSn;
	}

	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}

	@Override
	public String toString() {
		return super.toString() + ",AbstractMQTTMessage [msgSn=" + msgSn + ", deviceId=" + deviceSn + ", v=" + v + ", t=" + t + "]";
	}

}
