package ltd.nullpointer.tcp.core.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @author zhangweilin
 * @ClassName: TcpUpMessage.java
 * @Description: TCP上行消息报文
 * @date 2018年2月1日 上午10:46:55
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TcpDownMessage {

    /**
     * 头
     *
     * @return
     */
    @AliasFor("value")
    TcpHeader header();

    /**
     * 下发的消息对应的回复报文类型, 如果为空，则表示不会回复，或不用理会回复
     * @return
     */
    String replyType() default "";


    /**
     * 类型
     *
     * @return
     */
    TcpType type();

    /**
     * TCP字段长度
     *
     * @return
     */
    TcpMessageLength length() default @TcpMessageLength;

    /**
     * 保留字
     *
     * @return
     */
    TcpReservedWord reservedWord() default @TcpReservedWord();

    /**
     * 是否持久化, 默认为true
     * @return
     */
    boolean isPersistence() default true;


    /**
     * 校验类型,默认为BCC
     *
     * @return
     */
    CheckType checkType() default CheckType.BCC;

    enum CheckType {
        BCC, CS, CRC16, CRC32
    }
}
