package ltd.nullpointer.tcp.core.exception;

import ltd.nullpointer.tcp.core.constant.TCPEnum.ErrorCode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * 
 * @ClassName: MessageResolverException
 * @Description: 报文解析异常
 * @author zhangweilin
 * @date 2017年12月19日 下午4:54:48
 *
 */ 
public class MessageSerializerException extends RuntimeException {
	public Log log = LogFactory.getLog(this.getClass());
	/**
	 * 
	 */
	private static final long serialVersionUID = -3877531184268150162L;
	
	private int errCode;
	
	private ErrorCode errorCode;

	public MessageSerializerException(int errCode,String message, String... paras) {
		super(String.format(message, paras));
		this.errCode=errCode;
		log.error(String.format(message, paras));
	}

	public MessageSerializerException(int errCode,String message) {
		super(message);
		this.errCode=errCode;
		log.error(message);
	}

	public MessageSerializerException(int errCode,String message, Throwable e, String... paras) {
		super(String.format(message, paras), e);
		this.errCode=errCode;
		log.error(String.format(message, paras), e);
	}

	public MessageSerializerException(int errCode,String message, Throwable e) {
		super(message, e);
		this.errCode=errCode;
		log.error(message, e);
	}
	
	public MessageSerializerException(ErrorCode errorCode, String... paras) {
		super(String.format(errorCode.getName(), paras));
		this.errCode=errorCode.getErrCode();
		log.error(String.format(errorCode.getName(), paras));
	}
	
	public MessageSerializerException(ErrorCode errorCode,String message) {
		super(message);
		this.errCode=errorCode.getErrCode();
		log.error(message);
	}
	
	public MessageSerializerException(ErrorCode errorCode, Throwable e, String... paras) {
		super(String.format(errorCode.getName(), paras), e);
		this.errCode=errorCode.getErrCode();
		log.error(String.format(errorCode.getName(), paras), e);
	}
	
	public MessageSerializerException(ErrorCode errorCode, Throwable e) {
		super(errorCode.getName(), e);
		this.errCode=errorCode.getErrCode();
		log.error(errorCode.getName(), e);
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

}
