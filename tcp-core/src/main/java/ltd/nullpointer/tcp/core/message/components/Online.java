package ltd.nullpointer.tcp.core.message.components;


import lombok.Data;
import ltd.nullpointer.tcp.core.annotation.TcpUpMessage;
import ltd.nullpointer.tcp.core.annotation.TcpUpOffset;
import ltd.nullpointer.tcp.core.annotation.TcpRequired;
import ltd.nullpointer.tcp.core.message.AbstractNPiotMessage;

/**
 * @author zhangweilin
 * @ClassName: Online.java
 * @Description: 上线，首次上线为激活,此处的成员属性定义顺序不能乱变,定义的顺序就是tcp报文顺序
 * @date 2018年2月1日 上午10:18:26
 */
@TcpUpMessage(messageCode = "msgCode2Online")
@Data
public class Online extends AbstractNPiotMessage {

    /**
     *
     */
    private static final long serialVersionUID = -6372141403245631723L;

    /**
     * 固件版本号 1.0
     */
    @TcpUpOffset(start = 1, end = 3)
    private String softVer;
    /**
     * 通讯协议版本号 1.0
     */
    @TcpUpOffset(start = 4, end = 6)
    private String protocolVer;
    /**
     * 固件基线版本号 1.0
     */
    @TcpUpOffset(start = 7, end = 9)
    private String baselineVer;
    /**
     * mcu版本号 1.0
     */
    @TcpUpOffset(start = 10, end = 12)
    private String mcuVer;
    /**
     * 经度
     */
    @TcpUpOffset(start = 13, end = 23)
    @TcpRequired(false)
    private String lon;

    // 以下为可选，即下面三项，要么全有，要么全没有
    /**
     * 纬度
     */
    @TcpUpOffset(start = 24, end = 34)
    @TcpRequired(false)
    private String lan;

//    public Online() {
//        super.setMsgCode(MSG_CODE);
//    }


}
