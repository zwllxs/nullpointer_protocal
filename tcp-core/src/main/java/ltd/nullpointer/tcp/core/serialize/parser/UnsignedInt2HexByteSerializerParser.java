package ltd.nullpointer.tcp.core.serialize.parser;

import com.boot2.core.utils.ByteUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 正负int到16进制字节
 * @date 2019/12/9
 */
@Component
public class UnsignedInt2HexByteSerializerParser implements SerializerParser<Integer> {

    /**
     * 按具体规则序列化具体内容,
     *
     * @param integer
     * @param instance
     * @param paramMap
     * @return
     */
    @Override
    public byte[] serialize(Integer integer, Object instance, Map<String, Object> paramMap) {
        Integer length = (Integer) paramMap.get("byteLength");
        return ByteUtils.unsignedInt2HexByte(integer,length);
    }
}
