package ltd.nullpointer.tcp.core.serialize;


import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;

/**
 * 默认序列化器
 * 
 * @ClassName: DefaultMessageSerializer.java
 * @Description:
 * @author zhangweilin
 * @date 2018年1月30日 下午3:22:01
 *
 */
public class DefaultMessageSerializer implements MessageSerializer {

	@Override
	public ByteBuf serialize(AbstractTCPMessage abstractMessage) {
		int length = abstractMessage.getHeader().length + abstractMessage.getPayloadByteLength() + abstractMessage.getTail().length;
//		ByteBuffer messageByteBuffer = ByteBuffer
//				.allocate(length);
//		messageByteBuffer.put(abstractMessage.getHeader());
//		messageByteBuffer.put(abstractMessage.getPayloadByte());
//		messageByteBuffer.put(abstractMessage.getTail());

		ByteBuf byteBuf = PooledByteBufAllocator.DEFAULT.directBuffer(length);
		byteBuf.writeBytes(abstractMessage.getHeader());
		byteBuf.writeBytes(abstractMessage.getPayloadByte());
		byteBuf.writeBytes(abstractMessage.getTail());
		return byteBuf;
	}

	/**
	 * 默认需要走加密流程
	 */
	@Override
	public Boolean isNeedEncrypt() {
		return true;
	}

}
