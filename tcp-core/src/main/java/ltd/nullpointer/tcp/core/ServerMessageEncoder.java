package ltd.nullpointer.tcp.core;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;
import org.springframework.stereotype.Component;

/**
 * 
 * @Description:
 * @author zhangweilin
 * @date 2018年1月28日 下午8:29:27
 *
 */
@Component
public class ServerMessageEncoder extends MessageEncoder {
	@Override
	public void encode(ChannelHandlerContext ctx, AbstractTCPMessage abstractMessage, ByteBuf out) throws Exception {
		super.encode(ctx, abstractMessage, out);
	}
}
