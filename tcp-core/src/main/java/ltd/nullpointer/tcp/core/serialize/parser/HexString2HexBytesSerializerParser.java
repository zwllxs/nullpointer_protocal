package ltd.nullpointer.tcp.core.serialize.parser;

import ltd.nullpointer.tcp.core.constant.TCPEnum;
import ltd.nullpointer.tcp.core.exception.MessageSerializerException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 16进制字符串到字节
 * @date 2019/12/9
 */
@Component
public class HexString2HexBytesSerializerParser implements SerializerParser<String> {


    /**
     * 按具体规则序列化具体内容
     *
     * @param s
     * @param instance
     * @param paramMap
     * @return
     */
    @Override
    public byte[] serialize(String str, Object instance, Map<String, Object> paramMap) {
        try {
            byte[] byteArr = Hex.decodeHex(str.toCharArray());
            return byteArr;
        } catch (DecoderException e) {
            e.printStackTrace();
            throw new MessageSerializerException(TCPEnum.ErrorCode.err10008.getErrCode(), TCPEnum.ErrorCode.err10008.getName() + ",str: " + str);
        }
    }
}
