package ltd.nullpointer.tcp.core.resolver;

import lombok.Data;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/7
 */
public abstract class AbstractTCPMessageClassFieldEntry {

    /**
     * 解析组件缓存
     */
    protected Map<String, ClassFieldEntry> classFieldEntryMap;

    /**
     * 系统初始化时，会根据msgCode,缓存一对应的class以及对应的属性名称列表
     *
     * @param msgCodeFieldListMap
     */
    public void setClassFieldEntryMap(Map<String, ClassFieldEntry> classFieldEntryMap) {
        if (null != this.classFieldEntryMap && !this.classFieldEntryMap.isEmpty()) {
            throw new IllegalAccessError("classFieldEntryMap has been initialized");
        }
        this.classFieldEntryMap = classFieldEntryMap;
    }



    /**
     * @author zhangweilin
     * @date 2019/12/7 23:19
     * @Description:
     */
    @Data
    public static class ClassFieldEntry {

        /**
         * 对应的class
         */
        private Class<?> clazz;

        private List<Field> fieldList;
    }


}
