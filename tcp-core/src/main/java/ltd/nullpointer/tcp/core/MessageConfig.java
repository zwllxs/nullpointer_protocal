package ltd.nullpointer.tcp.core;

import com.boot2.core.utils.ByteUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

/**
 * @author zhangweilin
 * @Description: 配置报文的头和是否走加解密通道
 * @date 2019/12/13
 */
@Component
@Configuration
@ConfigurationProperties("tcp.message")
@Data
public class MessageConfig {

    private Set<String> headers;

    private Set<byte[]> headerByteArr;

    @PostConstruct
    public void init() {
        headerByteArr = new HashSet<>();
        for (String header : headers) {
            headerByteArr.add(ByteUtils.hexString2Bytes(header));
        }
    }

//    /**
//     *
//     */
//    private Map<String, Boolean> messagePropertiesMap;
//
//
//    @Data
//    public static class MessageProperties {
//
//        /**
//         * 字节头
//         */
//        private byte[] begin;
//
//        /**
//         * 是否加密
//         */
//        private boolean isNeedEncrypt;
//
//    }
}
