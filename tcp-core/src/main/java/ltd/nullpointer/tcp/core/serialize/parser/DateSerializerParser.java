package ltd.nullpointer.tcp.core.serialize.parser;

import com.boot2.core.utils.ByteUtils;
import com.boot2.core.utils.DateUtil;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 时间解析,
 * @date 2019/12/9
 */
@Component
public class DateSerializerParser implements SerializerParser<Date> {

    /**
     * 按具体规则序列化具体内容,此通过参数塞入转换的中间值，
     *
     * @param date
     * @param instance
     * @param paramMap
     * @return 固定返回null
     */
    @Override
    public byte[] serialize(Date date, Object instance, Map<String, Object> paramMap) {
        String dateFormat = (String) paramMap.get("dateFormat");
        String charsetName = (String) paramMap.get("charsetName");
        String dateStr = DateUtil.formatDate(date, dateFormat);
//        byte[] bytes = ByteUtils.getBytes(dateStr, charsetName);
        byte[] bytes = ByteUtils.hexString2Bytes(dateStr);
        return bytes;
    }
}
