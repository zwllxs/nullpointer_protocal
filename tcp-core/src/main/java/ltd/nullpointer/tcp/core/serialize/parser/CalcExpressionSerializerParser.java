package ltd.nullpointer.tcp.core.serialize.parser;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author zhangweilin
 * @Description: 表达式计算
 * @date 2019/12/9
 */
@Component
public class CalcExpressionSerializerParser implements SerializerParser<Object> {

    /**
     * 提取出所有的变量，如12+num1-age*height/90*(_last_n89ame65-$abc-(_ref+3.6))*math.pow(10, -3)/math.log10(abc),得到
     * num1
     * age
     * height
     * _last_n89ame65
     * $abc
     * _ref
     * math.pow
     * math.log10
     * abc
     */
    private final Pattern pattern = Pattern.compile("([a-zA-Z$_]\\w+)(\\.[a-zA-Z$_]\\w+)?");

    /**
     * 内置函数，不能被替换,故解析实体在给属性命名时，要避开函数名
     */
    private final List<String> formulaList = Arrays.asList("sysdate", "rand", "print", "println", "now", "long", "double", "str", "date_to_string",
            "string_to_date", "string.contains", "string.byteLength", "string.startsWith", "string.endsWith", "string.substring", "string.indexOf",
            "string.split", "string.join", "string.replace_first", "string.replace_all", "math.abs", "math.sqrt", "math.pow", "math.log", "math.log10",
            "math.sin", "math.cos", "math.tan", "map", "filter", "count", "include", "sort", "reduce", "seq.eq", "seq.neq", "seq.gt", "seq.ge", "seq.lt",
            "seq.le", "seq.nil", "seq.exists");


    /**
     * 按具体规则序列化具体内容
     *
     * @param o
     * @param instance
     * @param paramMap
     * @return
     */
    @Override
    public byte[] serialize(Object o, Object instance, Map<String, Object> paramMap) {
        //todo 后续实现
        return null;
    }

    /**
     * 只在此类中使用，返回类型
     */
    public enum ReturnType {
        /**
         * 延迟计算，即首次遍历如果还有未计算的表达式，则延迟到后面计算
         */
        Delay,Continue;
    }
}
