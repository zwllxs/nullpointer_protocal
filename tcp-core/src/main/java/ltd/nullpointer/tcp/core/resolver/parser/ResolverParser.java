package ltd.nullpointer.tcp.core.resolver.parser;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 解析插件，细化解析，可自由扩充, 如果计算出来的值，不想再重新赋给字段，则要返回为null
 * @date 2019/12/9
 */
public interface ResolverParser<T> {

    /**
     * 按具体规则解析具体内容
     *
     * @param byteArr
     * @return
     */
    T resolve(byte[] byteArr,Object obj, Map<String, Object> paramMap);
}
