package ltd.nullpointer.tcp.core.annotation;

import ltd.nullpointer.tcp.core.constant.TCPEnum;

import java.lang.annotation.*;

/**
 *
 * @Description: TCP字段长度
 * @author zhangweilin
 * @date 2018年2月1日 上午10:46:33
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TcpMessageLength {

	/**
	 * 占的字节数
	 * @return
	 */
	int byteLength() default 2;


	/**
	 * 默认为16进制字符串转字节
	 * @return
	 */
	TCPEnum.SerializerType serializerType() default TCPEnum.SerializerType.int2HexByte;

	/**
	 * 报文长度字节所在位置索引
	 * @return
	 */
	int index() default 3;

}
