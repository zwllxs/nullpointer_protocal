package ltd.nullpointer.tcp.core;

/**
 * 
 * @ClassName: EncryType.java
 * @Description:  EncryType
 * @author zhangweilin
 * @date 2018年1月22日 下午4:10:58
 *
 */
public enum EncryType {
	RSA_AES,AES
}
