package ltd.nullpointer.tcp.core.message;

import java.io.Serializable;

/**
 * 方舟平台抽象消息
 *
 * @Description:
 * @CreateDate:2018年4月26日 下午3:29:21
 * @Author:zhangweilin
 */
public class AbstractNPiotMessage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -2515019389232651011L;
    /**
     * 消息类型,一个值对应一个解析规则, 它可以是平台内置组件，也可以是用户自定义协议的消息唯一标识符
     */
    private String msgCode;

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }
}
