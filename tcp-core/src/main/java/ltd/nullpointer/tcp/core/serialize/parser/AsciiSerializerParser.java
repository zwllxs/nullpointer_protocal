package ltd.nullpointer.tcp.core.serialize.parser;

import com.boot2.core.utils.ByteUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: ascii
 * @date 2019/12/9
 */
@Component
public class AsciiSerializerParser implements SerializerParser<String> {

    /**
     * 按具体规则序列化具体内容
     *
     * @param s
     * @return
     */
    @Override
    public byte[] serialize(String str, Object instance, Map<String, Object> paramMap) {
        String charsetName = (String) paramMap.get("charsetName");
        byte[] byteArr = ByteUtils.getBytes(str, charsetName);
        return byteArr;
    }
}
