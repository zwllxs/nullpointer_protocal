package ltd.nullpointer.tcp.core;


import io.netty.buffer.ByteBuf;

/**
 * @author zhangweilin
 * @date 2019/12/8 16:37
 * @Description:
 */
public interface TCPMessageHandler<T> {


    /**
     * 二次处理tcp消息
     *
     * @param byteBuf
     * @param npiotMessage
     * @return
     */
    Object hand(ByteBuf byteBuf, T t);

}
