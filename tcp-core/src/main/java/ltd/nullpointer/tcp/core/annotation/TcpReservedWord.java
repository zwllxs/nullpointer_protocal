package ltd.nullpointer.tcp.core.annotation;

import ltd.nullpointer.tcp.core.constant.TCPEnum;

import java.lang.annotation.*;

/**
 *
 * @Description: 保留字
 * @author zhangweilin
 * @date 2018年2月1日 上午10:46:33
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TcpReservedWord {

	/**
	 *  保留字,16进制字符串
	 * @return
	 */
	String value() default "00";


	/**
	 * 默认为16进制字符串转字节
	 * @return
	 */
	TCPEnum.SerializerType serializerType() default TCPEnum.SerializerType.hexString2HexBytes;

	/**
     * 保留字 字节所在位置索引
	 * @return
     */
	int index() default 4;

}
