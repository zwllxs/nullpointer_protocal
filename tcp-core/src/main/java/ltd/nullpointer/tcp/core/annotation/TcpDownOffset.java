package ltd.nullpointer.tcp.core.annotation;

import ltd.nullpointer.tcp.core.constant.TCPEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author zhangweilin
 * @Description: TCP偏移量
 * @date 2018年2月1日 上午10:46:33
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface TcpDownOffset {

    /**
     * 开始位
     *
     * @return
     */
    int start() default -1;

    /**
     * 结束位
     *
     * @return
     */
    int end() default -1 ;


    /**
     * 默认为16进制字符串转字节
     *
     * @return
     */
    TCPEnum.SerializerType serializerType() default TCPEnum.SerializerType.hexString2HexBytes;

    /**
     * 如果是日期时间，此为序列化的格式
     * @return
     */
    String dateFormat() default "";

    /**
     * 字符集编码,ascii码时才有效
     * @return
     */
    String charsetName() default  "GB2312";

    /**
     * 数值计算表达式，只有在基础类型和BigDecimal才有效，
     * 当前值用this表示，如this*0.01。
     * 如果是整数计算，请在数字后面加上N,如this*999999999999N
     * 如果是浮点运算，为了不丢失精度，请在数字后面加上M,如this*0.999999999999M
     * 如果啥也不加，默认按double计算
     * @return
     */
    String calcExpression() default "";
}
