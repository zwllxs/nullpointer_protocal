package ltd.nullpointer.tcp.core.resolver.parser;

import com.boot2.core.utils.ByteUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 取高四位得int
 * @date 2019/12/9
 */
@Component
public class Height4ToIntResolverParser implements ResolverParser<Integer> {

    /**
     * @param byteArr
     * @return
     */
    @Override
    public Integer resolve(byte[] byteArr,Object obj, Map<String, Object> paramMap) {
        int value = ByteUtils.getHeight4(byteArr[0]);
        return value;
    }
}
