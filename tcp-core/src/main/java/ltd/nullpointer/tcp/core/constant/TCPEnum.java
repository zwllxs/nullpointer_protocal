package ltd.nullpointer.tcp.core.constant;

/**
 * @author zhangweilin
 * @Description: 错误代码
 * @date 2018年1月13日 下午2:10:06
 */
public class TCPEnum {

    /**
     * @author zhangweilin
     * @ClassName: ErrorCode
     * @Description: 类型
     * @date 2017年12月28日 下午7:25:37
     */
    public static enum ErrorCode {
        err10001("根据指定的密钥对1,找不到相应的产品"),
        err10002("非法报文"),
        err10003("校验失败"),
        err10004("msgCode解析失败"),
        err10005("msgSn解析失败"),
        err10006("设备id长度解析失败"),
        err10007("isLanMsg解析失败"),
        err10008("字段读取失败"),
        err10010("字段解析失败"),
        err10009("msgCode未定义"),
        err10011("msgCode can not be null"),
        err10012("设备尚未上线"),
        err10013("设备无须重复上线"),
        err10014("找不到相应设备，无效设备id，有可能该设备id尚未在平台注册"),
        err10015("服务端未知错误"),
        err10016("如果为指定长度类型，则需要指定目标字段，且只能指定一个字段"),
        err10017("设备id与使用的公钥密钥不对应"),
        err10018("没有数据");

        private String name;

        private ErrorCode(String name) {
            this.name = name;
        }

        public static String getName(int index) {
            for (ErrorCode c : ErrorCode.values()) {
                if (Integer.valueOf(c.name().replace("err", "")) == index) {
                    return c.name;
                }
            }
            return null;
        }

        public int getErrCode() {
            return Integer.valueOf(this.name().replace("err", ""));
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * @author zhangweilin
     * @ClassName: MsgCode
     * @Description: 消息类型
     * @date 2017年12月28日 下午7:25:37
     */
    public static enum MsgCode {
        msgCode63SysErr("系统错误"),
        msgCode2Online("上线激活"),
        msgCode6("设备在线状态查询"),
        msgCode16("设备上线成功通知"),
        msgCode17("设备下线成功通知");

        private String name;

        private MsgCode(String name) {
            this.name = name;
        }

        public static String getName(String code) {
            for (MsgCode c : MsgCode.values()) {
                if (Integer.valueOf(c.name().replaceAll("\\D", "")) .equals(code)) {
                    return c.name;
                }
            }
            return null;
        }

        public static MsgCode getMsgCode(String code) {
            for (MsgCode c : MsgCode.values()) {
                if (Integer.valueOf(c.name().replaceAll("\\D", "")).equals(code)) {
                    return c;
                }
            }
            return null;
        }

        public int getMessageCode() {
            return Integer.valueOf(this.name().replaceAll("\\D", ""));
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 解析枚举
     */
    public static enum ResolveType{
        hexByte2Int,
        hexByte2UnsignedInt,
        hexBytes2HexString,
        ascii,
        manually  //此为手工解析
    }

    /**
     * 序列化枚举
     */
    public static enum SerializerType{
        int2HexByte,
        unsignedInt2HexByte,
        hexString2HexBytes,
        ascii,
        manually  //此为手工解析
    }

}
