package ltd.nullpointer.tcp.core.annotation;

import ltd.nullpointer.tcp.core.constant.TCPEnum;

import java.lang.annotation.*;

/**
 * @author zhangweilin
 * @Description: 类型
 * @date 2018年2月1日 上午10:46:55
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TcpType {

    /**
     *  类型描述,16进制字符串
     * @return
     */
    String value();

    /**
     * 默认为16进制字符串转字节
     * @return
     */
    TCPEnum.SerializerType serializerType() default TCPEnum.SerializerType.hexString2HexBytes;

    /**
     *  命令字（命令类型）默认为2
     * @return
     */
    int index() default  2;

}
