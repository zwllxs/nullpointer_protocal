package ltd.nullpointer.tcp.core.serialize.parser;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 序列化插件，细化解析，可自由扩充, 如果计算出来的值，不想再重新赋给字段，则要返回为null
 * @date 2019/12/9
 */
public interface SerializerParser<T> {

    /**
     * 按具体规则序列化具体内容
     *
     * @param t
     * @return
     */
    byte[] serialize(T t,Object instance, Map<String, Object> paramMap);
}
