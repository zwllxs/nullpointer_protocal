package ltd.nullpointer.tcp.core.annotation;

import ltd.nullpointer.tcp.core.constant.TCPEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author zhangweilin
 * @Description: TCP上行消息报文
 * @date 2018年2月1日 上午10:46:55
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface TcpHeader {

    /**
     * 头，16进制字符串
     *
     * @return
     */
    String value();

    /**
     * 默认为16进制字符串转字节
     *
     * @return
     */
    TCPEnum.SerializerType serializerType() default TCPEnum.SerializerType.hexString2HexBytes;

    /**
     * 起始位置索引，头默认为1
     *
     * @return
     */
    int index() default 1;

}
