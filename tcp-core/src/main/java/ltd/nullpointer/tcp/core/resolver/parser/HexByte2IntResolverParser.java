package ltd.nullpointer.tcp.core.resolver.parser;

import com.boot2.core.utils.ByteUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 16进制字节到int
 * @date 2019/12/9
 */
@Component
public class HexByte2IntResolverParser implements ResolverParser<Integer> {

    /**
     * @param byteArr
     * @return
     */
    @Override
    public Integer resolve(byte[] byteArr,Object obj, Map<String, Object> paramMap) {
        return ByteUtils.hexByte2Int(byteArr);
    }
}
