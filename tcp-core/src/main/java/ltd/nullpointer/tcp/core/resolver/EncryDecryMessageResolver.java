package ltd.nullpointer.tcp.core.resolver;

import com.boot2.core.utils.ByteUtils;
import io.netty.buffer.ByteBuf;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;
import ltd.nullpointer.tcp.core.message.EncryDecryMessage;

/**
 * 密钥握手消息
 * 
 * @ClassName: EncryDecryMessageAnalyzer.java
 * @Description:
 * @author zhangweilin
 * @date 2018年1月21日 下午12:10:29
 *
 */
public class EncryDecryMessageResolver implements MessageResolver {

	@Override
	public AbstractTCPMessage resolver(ByteBuf byteBuf) {
		// 读取密钥类型
		final byte[] typeByte = new byte[1];
		byteBuf.readBytes(typeByte);
		// String type = ByteUtils.byteArr2HexStr(typeByte);
		String type = ByteUtils.getString(typeByte);
		EncryDecryMessage.KeyType keyType = EncryDecryMessage.KeyType.getValue(Integer.valueOf(type));
		EncryDecryMessage encryDecryMessage = new EncryDecryMessage(keyType);

		// 读取剩下全部字节
		int contentLength = byteBuf.readableBytes();
		final byte[] contentByte = new byte[contentLength];
		byteBuf.readBytes(contentByte);
		encryDecryMessage.setPayloadByte(contentByte);
		return encryDecryMessage;
	}

	@Override
	public Boolean isNeedEncrypt() {
		// TODO Auto-generated method stub
		return false;
	}
}
