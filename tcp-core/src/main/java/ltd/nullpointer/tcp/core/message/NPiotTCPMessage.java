package ltd.nullpointer.tcp.core.message;

import org.apache.commons.lang3.ArrayUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @ClassName: NPiotTCPMessage.java
 * @Description: 正文消息
 * @author zhangweilin
 * @date 2018年1月21日 上午10:58:47
 *
 */
public class NPiotTCPMessage extends AbstractTCPMessage {

	/** 
	 *  
	 */
	private static final long serialVersionUID = -1719445458808827286L;

	/**
	 * 正文消息响应头
	 */
	public static final byte[] ARK_HEADER_BYTE = new byte[] { (byte) 0xA5, (byte) 0x41, (byte) 0x72, (byte) 0x6B,
			(byte) 0xAA };
//	public static final byte[] ARK_HEADER_BYTE = new byte[] { (byte) 0x55, (byte) 0x7A  };

	private Map<String, Object> dataMap = new HashMap<>();

	@Override
	public byte[] getHeader() {
		if (ArrayUtils.isEmpty(header)) {
			header = ARK_HEADER_BYTE;
		}
		return header;
	}

	/**
	 * 消息序号
	 */
	private Integer msgSn = 0;

//	/**
//	 * 设备id
//	 */
//	private String deviceId;

	/**
	 * 是否为局域网
	 */
	private Boolean isLanMsg;

	public void put(String key, Object value) {
		this.dataMap.put(key, value);
	}

	public Object get(String key) {
		return this.dataMap.get(key);
	}
	
	/**
	 * 消息的唯一Id
	 * @return
	 */
	public String getMessageId() {
		return (getMsgCode()==null?"":getMsgCode())+"_"+(msgSn==null?"":msgSn)+"_"+(deviceSn==null?"":deviceSn);
//		if (StringUtils.isEmpty(messageId)) {
//			messageId=RandomUtils.randomString(16);
//			return messageId;
//		}
//		return messageId;
	}


	public Integer getMsgSn() {
		return msgSn;
	}

	public void setMsgSn(Integer msgSn) {
		this.msgSn = msgSn;
	}

//	@Override
//	public String getDeviceId() {
//		return deviceId;
//	}
//
//	@Override
//	public void setDeviceId(String deviceId) {
//		this.deviceId = deviceId;
//	}
//
//	public Boolean getIsLanMsg() {
//		return isLanMsg;
//	}
//
//	public void setIsLanMsg(Boolean isLanMsg) {
//		this.isLanMsg = isLanMsg;
//	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}
}
