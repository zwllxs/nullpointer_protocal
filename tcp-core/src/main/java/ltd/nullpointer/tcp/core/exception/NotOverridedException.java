package ltd.nullpointer.tcp.core.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * 
 * @ClassName: NotOverridedException
 * @Description: 方法未实现异常
 * @author zhangweilin
 * @date 2017年12月19日 下午4:54:48
 *
 */ 
public class NotOverridedException extends RuntimeException {
	public Log log = LogFactory.getLog(this.getClass());
	/**
	 * 
	 */
	private static final long serialVersionUID = -3877531184268150162L;

	public NotOverridedException(String message, String... paras) {
		super(String.format(message, paras));
		log.error(String.format(message, paras));
	}

	public NotOverridedException(String message) {
		super(message);
		log.error(message);
	}

	public NotOverridedException(String message, Throwable e, String... paras) {
		super(String.format(message, paras), e);
		log.error(String.format(message, paras), e);
	}

	public NotOverridedException(String message, Throwable e) {
		super(message, e);
		log.error(message, e);
	}

}
