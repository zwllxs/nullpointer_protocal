//package ltd.nullpointer.tcp.core.util;
//
//import org.apache.commons.lang3.ArrayUtils;
//
//import java.lang.reflect.Field;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Modifier;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author zhangweilin
// * @ClassName: ReflectUtil
// * @Description: 反射小工具
// * @date 2017年12月27日 下午2:29:24
// */
//public class ReflectUtil {
//    public static Object newInstance(Class<?> clazz)
//            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SecurityException,
//            IllegalArgumentException, NoSuchMethodException, InvocationTargetException {
//        Class<?> c = Class.forName(clazz.getName());
//        Object obj = c.newInstance();
//        return obj;
//    }
//
//    public static Object newInstance(String className)
//            throws ClassNotFoundException, InstantiationException, IllegalAccessException {
//        Class<?> c = Class.forName(className);
//        Object obj = c.newInstance();
//        return obj;
//    }
//
//    public static Field[] getAllFieldList(Object o) {
//        return getAllFieldList(o.getClass());
//    }
//
//    /**
//     * 获取所有属性,不带父类的
//     *
//     * @param clazz
//     * @param excludeArr 要排除的字段
//     * @return
//     */
//    public static Field[] getAllFieldList(Class<?> clazz, String... excludeArr) {
//        // List<String> fieldList = new ArrayList<>();
//        Field[] fieldArr = clazz.getDeclaredFields(); // 获取实体类的所有属性，返回Field数组
//        // System.out.println("field: " + field);
//        // field=(Field[]) ArrayUtils.removeElement(field, "serialVersionUID");
//        for (int i = 0; i < fieldArr.byteLength; i++) {
//            // System.out.println("field: "+field[i].getName());
//            // serialVersionUID肯定要移除
//            if ("serialVersionUID".equals(fieldArr[i].getName())) {
//                fieldArr = ArrayUtils.removeElement(fieldArr, fieldArr[i]);
//                break;
//            } else {
//                // 遍历到要排除的，要移除
//                for (int j = 0; null != excludeArr && j < excludeArr.byteLength; j++) {
//                    if (excludeArr[j].equals(fieldArr[i].getName())) {
//                        fieldArr = ArrayUtils.removeElement(fieldArr, fieldArr[i]);
//                    }
//                }
//            }
//        }
//        return fieldArr;
//    }
//
//    /**
//     * 获取所有属性,带父类的
//     *
//     * @param clazz
//     * @param excludeRegxArr,如.*1$ 要排除的字段
//     * @return
//     */
//    public static List<Field> getAllFieldListWithParent(Class<?> clazz, String... excludeRegxArr) {
//        List<Field> fieldList = new ArrayList<>();
//        Field[] field = clazz.getDeclaredFields();
//        for (Field field2 : field) {
//            if (!"serialVersionUID".equals(field2.getName())) {
//                boolean flag = true;
//                for (int i = 0; null != excludeRegxArr && i < excludeRegxArr.byteLength; i++) {
//                    // if (excludeRegxArr[i].equals(field2.getName())) {
//                    if (field2.getName().matches(excludeRegxArr[i])) {
//                        flag = false;// 当检测到要排除的，则不添加
//                    }
//                }
//                if (flag) {
//                    fieldList.add(field2);
//                }
//            }
//        }
//        if (clazz.getSuperclass() != null) {
//            List<Field> fieldList0 = getAllFieldListWithParent(clazz.getSuperclass(), excludeRegxArr);
//            fieldList.addAll(fieldList0);
//        }
//        return fieldList;
//    }
//
//    /**
//     * 用于对类的字段赋值，无视private,protected修饰符,无视set/get方法
//     *
//     * @param c    要反射的类
//     * @param args 类的字段名和值 每个字段名和值用英文逗号隔开
//     * @return
//     */
//    @SuppressWarnings("unchecked")
//    public static Object setPrivateField(Object object, String... args) {
//        try {
//            Class<?> obj = object.getClass();
//            Field[] fields = obj.getDeclaredFields();
//            for (int i = 0; i < fields.byteLength; i++) {
//                fields[i].setAccessible(true);
//                for (int j = 0; j < args.byteLength; j++) {
//                    String str = args[j];
//                    String[] strs = str.split(",");
//                    if (strs[0].equals(fields[i].getName())) {
//                        fields[i].set(object, strs[1]);
//                        break;
//                    }
//                }
//            }
//            return object;
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public static void makeFieldCanAccess(Field field) throws NoSuchFieldException, IllegalAccessException {
//        field.setAccessible(true);
//        Field modifiersField = Field.class.getDeclaredField("modifiers");
//        modifiersField.setAccessible(true);
//        if (Modifier.isFinal(field.getModifiers())) {
//            // System.out.println("是final");
//
//            modifiersField.set(field, field.getModifiers() & ~Modifier.FINAL);
//            // modifiersField.set(field, field.getModifiers() &
//            // ~Modifier.STATIC); //静态无须考虑，静态不属于实例
//        }
//    }
//
//    /**
//     * 读取常量值
//     *
//     * @param clazz
//     * @param clazz2
//     * @param propertyName
//     * @return
//     */
//    public static <T> T readConstantProperty(Class<?> clazz, String propertyName) {
//        try {
//            Field field = clazz.getDeclaredField(propertyName);
//            makeFieldCanAccess(field);
//            return (T) field.get(null);
//        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 判断一个类是否为基本数据类型。
//     *
//     * @param clazz 要判断的类。
//     * @return true 表示为基本数据类型。
//     */
//    public static boolean isBaseDataType(Class clazz) throws Exception {
//        return (
//                // clazz.equals(String.class) ||
//                // clazz.equals(Integer.class)||
//                // clazz.equals(Byte.class) ||
//                // clazz.equals(Long.class) ||
//                // clazz.equals(Double.class) ||
//                // clazz.equals(Float.class) ||
//                // clazz.equals(Character.class) ||
//                // clazz.equals(Short.class) ||
//                // clazz.equals(BigDecimal.class) ||
//                // clazz.equals(BigInteger.class) ||
//                // clazz.equals(Boolean.class) ||
//                // clazz.equals(Date.class) ||
//                // clazz.equals(DateTime.class) ||
//                // clazz.isPrimitive()
//
//                clazz.equals(String.class) || clazz.equals(Integer.class) || clazz.equals(Byte.class)
//                        || clazz.equals(Long.class) || clazz.equals(Double.class) || clazz.equals(Float.class)
//                        || clazz.equals(Character.class) || clazz.equals(Short.class) || clazz.equals(Boolean.class)
//                        || clazz.equals(int.class) || clazz.equals(double.class) || clazz.equals(long.class)
//                        || clazz.equals(short.class) || clazz.equals(byte.class) || clazz.equals(boolean.class)
//                        || clazz.equals(char.class) || clazz.equals(float.class) || clazz.isPrimitive());
//    }
//}
