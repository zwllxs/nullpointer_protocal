package ltd.nullpointer.tcp.core.serialize;

/**
 * 默认序列化器
 * 
 * @ClassName: DefaultMessageSerializer.java
 * @Description:
 * @author zhangweilin
 * @date 2018年1月30日 下午3:22:01
 *
 */
public class HlCompatibleTCPMessageDefaultSerializer extends DefaultMessageSerializer {

	/**
	 * 不需要走加密流程
	 */
	@Override
	public Boolean isNeedEncrypt() {
		return false;
	}

}
