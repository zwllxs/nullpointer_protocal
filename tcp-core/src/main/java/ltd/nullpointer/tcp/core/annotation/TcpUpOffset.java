package ltd.nullpointer.tcp.core.annotation;

import ltd.nullpointer.tcp.core.constant.TCPEnum;
import ltd.nullpointer.tcp.core.resolver.parser.HexBytes2HexStringResolverParser;
import ltd.nullpointer.tcp.core.resolver.parser.ResolverParser;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author zhangweilin
 * @Description: TCP偏移量
 * @date 2018年2月1日 上午10:46:33
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface TcpUpOffset {

    /**
     * 开始位
     *
     * @return
     */
    int start() default -1;

    /**
     * 结束位
     *
     * @return
     */
    int end() default -1;

    /**
     * 解析方式
     * @return
     */
    TCPEnum.ResolveType resolveType() default TCPEnum.ResolveType.hexBytes2HexString;

    /**
     * 如果是日期时间，此为解析格式
     * @return
     */
    String dateFormat() default "";

    /**
     * 字符集编码,ascii码时才有效
     * @return
     */
    String charsetName() default  "GB2312";

    /**
     * 数值计算表达式，只有在基础类型和BigDecimal才有效，
     * 当前值用this表示，如this*0.01。
     * 如果是整数计算，请在数字后面加上N,如this*999999999999N
     * 如果是浮点运算，为了不丢失精度，请在数字后面加上M,如this*0.999999999999M
     * 如果啥也不加，默认按double计算
     * @return
     */
    String calcExpression() default "";

    /**
     *
     * @return
     */
    boolean skip() default false;

    /**
     * 如果有重复使用，返回true
     *
     * @return
     */
    boolean repeat() default  false;

    /**
     * 如果有循环,循环的类型
     *
     * @return
     */
    Class<?> loopClass() default Object.class;

    /**
     * 默认要和resolveType一致
     * @return
     */
    Class<? extends ResolverParser<?>>[] resolverParser() default HexBytes2HexStringResolverParser.class;

    /**
     * 自定义转换器的参数对，参数对用json的格式，在没指定自定义转换器时，此注解无意义,此是数组，与resolverParser下标对应
     * @return
     */
    String[] resolverParserParamPair() default "";

}
