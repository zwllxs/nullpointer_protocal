package ltd.nullpointer.tcp.core.annotation;


import java.lang.annotation.*;

/**
 * 标识一个上报实体的标识字段
 */
@Documented  
@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.FIELD)  
public @interface DeviceSn {

    /**
     * 标识一个上报实体的标识字段,类似持久化中的主键字段
     * @return
     */
    boolean value() default true;
}
