package ltd.nullpointer.tcp.core.message;

/**
 * 云端错误代码,这指在解码器里云端自定义错误，需要返回给设备的，故它的值将传给ErrorMessage对象，再回复给设备
 * 
 * @ClassName: SysErrorMessage.java
 * @Description:
 * @author zhangweilin
 * @date 2018年1月21日 上午10:58:47
 *
 */
public class SysErrorMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3647295406475213127L;

	public SysErrorMessage(Integer errCode, String errMessage) {
		super();
		this.errCode = errCode;
		this.errMessage = errMessage;
	}

	public SysErrorMessage(Integer errCode) {
		super();
		this.errCode = errCode;
	}

	/**
	 * 错误代码
	 */
	private Integer errCode;

	/**
	 * 错误消息,此不生成报文
	 */
	private String errMessage;

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public void setErrCode(Integer errCode) {
		this.errCode = errCode;
	}

	public Integer getErrCode() {
		return errCode;
	}
}
