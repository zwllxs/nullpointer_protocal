package ltd.nullpointer.tcp.core;

/**
 * @author zhangweilin
 * @Description: 定义的tcp下发接口
 * @date 2019/12/20
 */
public interface TCPMessageProducer {

    /**
     * 发送给设备的实体
     *
     * @param deviceId
     * @param object
     */
    void sendToDevice(String deviceId, Object object);

    /**
     * 处理下发的回复消息
     *
     * @param object
     */
    void handReply(Object object);

}
