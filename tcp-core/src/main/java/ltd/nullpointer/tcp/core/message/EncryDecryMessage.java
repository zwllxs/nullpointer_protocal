package ltd.nullpointer.tcp.core.message;

import com.boot2.core.utils.ByteUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 * 
 * @ClassName: EncryDecryMessage.java
 * @Description: 密钥交换报文消息体
 * @author zhangweilin
 * @date 2018年1月19日 下午4:13:46
 *
 */
public class EncryDecryMessage extends AbstractTCPMessage {
	/**
	 * 密钥头
	 */
	public static final byte[] KEY_HEADER_BYTE = new byte[] { (byte) 0xA5, (byte) 0x4B, (byte) 0x45, (byte) 0x59,
			(byte) 0xAA };

	/**
	 * RSA1
	 */
	public static final byte[] RSA1_HEADER_BYTE = ArrayUtils.addAll(KEY_HEADER_BYTE,
			String.valueOf(KeyType.RSA1.index).getBytes());

	/**
	 * RSA2
	 */
	public static final byte[] RSA2_HEADER_BYTE = ArrayUtils.addAll(KEY_HEADER_BYTE,
			String.valueOf(KeyType.RSA2.index).getBytes());

	/**
	 * AES1
	 */
	public static final byte[] AES1_HEADER_BYTE = ArrayUtils.addAll(KEY_HEADER_BYTE,
			String.valueOf(KeyType.AES1.index).getBytes());

	/**
	 * AES2
	 */
	public static final byte[] AES2_HEADER_BYTE = ArrayUtils.addAll(KEY_HEADER_BYTE,
			String.valueOf(KeyType.AES2.index).getBytes());

	/**
	 * 密钥报文类型
	 * 
	 * @ClassName: EncryDecryMessage.java
	 * @Description:
	 * @author zhangweilin
	 * @date 2018年1月19日 下午4:30:31
	 *
	 */
	public static enum KeyType {
		RSA1(1), RSA2(2), AES1(3), AES2(4);

		private int index;

		private KeyType(int index) {
			this.index = index;
		}

		public static KeyType getValue(int index) {
			for (KeyType c : KeyType.values()) {
				if (c.getIndex() == index) {
					return c;
				}
			}
			return null;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}
	}

	public EncryDecryMessage(KeyType type) {
		super();
		this.type = type;
	}

	/**
	 * 公钥
	 */
	private String publicKey;

	/**
	 * 密钥
	 */
	private String privateKey;

	@Override
	public void setPayloadByte(byte[] payloadByte) {
		super.setPayloadByte(payloadByte);
		String content = ByteUtils.hexBytes2HexString(payloadByte);
//		String content = ByteUtils.byteArr2HexStr(payloadByte);
//		String content = ByteUtils.getString(payloadByte);
		super.setPayload(content);
		switch (this.type) {
		// RSA+AES加密方式，客户端给服务端发送RSA公钥,AES加密方式，客户端主动把AES密钥对的key1发送给服务端
		case RSA1:
		case AES1:
			setPublicKey(content);
			break;
		// RSA+AES加密方式，服务端给客户端回复AES密钥,PS:此密钥必须由第一步接收到的RSA公钥加密后再传输,此只在RSA生成方调用
		case RSA2:
		case AES2:
			setPrivateKey(content);
			break;
		default:
			break;
		}

	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
		this.payload = publicKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
		this.payload = privateKey;
	}

	/**
	 * 当前密钥报文类型
	 */
	private KeyType type;

	@Override
	public byte[] getHeader() {
		if (ArrayUtils.isEmpty(header)) {
			header = ArrayUtils.addAll(KEY_HEADER_BYTE, String.valueOf(type.index).getBytes());
		}
		return header;
	}

	public KeyType getType() {
		return type;
	}

	public void setType(KeyType type) {
		this.type = type;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}
}
