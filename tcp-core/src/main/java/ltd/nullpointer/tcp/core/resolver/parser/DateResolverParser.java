package ltd.nullpointer.tcp.core.resolver.parser;

import com.boot2.core.utils.DateUtil;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 时间解析
 * @date 2019/12/9
 */
@Component
public class DateResolverParser implements ResolverParser<Date> {

    /**
     * @param byteArr
     * @return
     */
    @Override
    public Date resolve(byte[] byteArr,Object obj, Map<String, Object> paramMap) {
        String dateFormat = (String) paramMap.get("dateFormat");
        String value = (String) paramMap.get("value");
        Date date = DateUtil.getDateByStr(value, dateFormat);
        return date;
    }
}
