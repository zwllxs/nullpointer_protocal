package ltd.nullpointer.tcp.core;

/**
* @author zhangweilin
* @date 2018/7/4 17:30 
*/
public abstract class MessageRouter {

    public abstract void init();

}
