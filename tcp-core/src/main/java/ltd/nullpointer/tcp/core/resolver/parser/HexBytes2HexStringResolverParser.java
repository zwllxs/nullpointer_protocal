package ltd.nullpointer.tcp.core.resolver.parser;

import com.boot2.core.utils.ByteUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 16进制字符串到字符串
 * @date 2019/12/9
 */
@Component
public class HexBytes2HexStringResolverParser implements ResolverParser<String> {

    /**
     * @param byteArr
     * @return
     */
    @Override
    public String resolve(byte[] byteArr,Object obj, Map<String, Object> paramMap) {
        return ByteUtils.hexBytes2HexString(byteArr).trim();
    }
}
