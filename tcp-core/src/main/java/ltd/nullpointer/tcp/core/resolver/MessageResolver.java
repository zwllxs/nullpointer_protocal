package ltd.nullpointer.tcp.core.resolver;

import io.netty.buffer.ByteBuf;
import ltd.nullpointer.tcp.core.exception.MessageResolverException;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;

/**
 * 
 * @ClassName: MessageResolver.java
 * @Description: 消息编码解析器
 * @author zhangweilin
 * @date 2018年1月21日 下午12:08:50
 *
 */
public interface MessageResolver {

	/**
	 * 分析和包装消息
	 * 
	 * @param byteBuf
	 * @return
	 */
	AbstractTCPMessage resolver(ByteBuf byteBuf) throws MessageResolverException;

	/**
	 * 是否需要加密
	 * 
	 * @return
	 */
	Boolean isNeedEncrypt();
}
