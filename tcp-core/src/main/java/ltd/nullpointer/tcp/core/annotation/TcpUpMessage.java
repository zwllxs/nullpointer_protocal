package ltd.nullpointer.tcp.core.annotation;

import java.lang.annotation.*;

/**
 * @author zhangweilin
 * @ClassName: TcpUpMessage.java
 * @Description: TCP上行消息报文
 * @date 2018年2月1日 上午10:46:55
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TcpUpMessage {

    /**
     * 起始位置
     *
     * @return
     */
    int start() default 0;

    /**
     * 协议消息的唯一标识, 同一个报文可以有多种标识，以应对不同命令字，相同的报文格式
     *
     * @return
     */
    String[] messageCode();


    /**
     * 是否持久化, 默认为true
     * @return
     */
    boolean isPersistence() default true;

    /**
     * 确定唯一值的属性，可多个
     * @return
     */
    String[] uniqueProperties() default {};

    /**
     * 校验类型,默认为BCC
     * @return
     */
    CheckType checkType() default CheckType.BCC;

    enum CheckType {
        BCC, CS, CRC16, CRC32
    }
}
