package ltd.nullpointer.tcp.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 
 * @ClassName: TcpRequired.java
 * @Description: TCP是否必传, 当为true时，如果没传值，系统也会用0来填充补齐位数，
 *               当指定为false时，如果没有值，则不生成报文，不补齐位数,请慎用此注解，一般只有拼在报文最后的才允许为非必传，如果有多个非必传，则所有非必传必须同时为false,要么全部不标记,否则容易导致解析错位
 * @author zhangweilin
 * @date 2018年2月1日 上午10:46:33
 *
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface TcpRequired {
	boolean value() default true;
}
