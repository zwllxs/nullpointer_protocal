package ltd.nullpointer.tcp.core.serialize;

import io.netty.buffer.ByteBuf;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;

/**
 * 
 * @ClassName: MessageSerializer.java
 * @Description: 消息序列化器
 * @author zhangweilin
 * @date 2018年1月21日 下午12:08:50
 *
 */
public interface MessageSerializer {

	/**
	 * 将包序列化成字节序列
	 * 
	 * @param abstractTCPMessage
	 * @return
	 */
	ByteBuf serialize(AbstractTCPMessage<?> abstractTCPMessage);

	/**
	 * 是否需要加密
	 * 
	 * @return
	 */
	Boolean isNeedEncrypt();
}
