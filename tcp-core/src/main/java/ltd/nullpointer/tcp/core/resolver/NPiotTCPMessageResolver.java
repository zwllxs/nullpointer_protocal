package ltd.nullpointer.tcp.core.resolver;

import io.netty.buffer.ByteBuf;
import ltd.nullpointer.tcp.core.exception.MessageResolverException;
import ltd.nullpointer.tcp.core.message.AbstractTCPMessage;
import ltd.nullpointer.tcp.core.message.NPiotTCPMessage;

/**
 * @author zhangweilin
 * @Description: 平台业务消息
 * @date 2018年1月21日 下午12:10:29
 */
public abstract class NPiotTCPMessageResolver extends AbstractTCPMessageResolver {

    @Override
    public AbstractTCPMessage resolver(ByteBuf byteBuf) throws MessageResolverException {
//        // 读取剩下全部字节,剩下全为业务报文
////        int contentLength = byteBuf.readableBytes();
////        final byte[] contentByte = new byte[contentLength];
////        System.out.println("contentByte.byteLength = " + contentByte.byteLength);
////        byteBuf.readBytes(contentByte);
////        System.out.println("最终收到contentByte: " + Arrays.toString(contentByte));
//////		String content = ByteUtils.getString(contentByte);
////        String hexString = Hex.encodeHexString(contentByte);
////        // 解析msgCode 3位
////        System.out.println("NPMessageResolver.hexString: " + hexString);
////
////        int start = 0;
////        int byteLength = 3;
////
////        // msgCode
////        String msgCodeStr = StringUtils.substring(hexString, start, byteLength);
////        Integer msgCode;
////        try {
////            msgCode = Integer.valueOf(msgCodeStr);
////        } catch (NumberFormatException e) {
////            e.printStackTrace();
////            throw new MessageResolverException(ErrorCode.err10004.getErrCode(), ErrorCode.err10004.getName(), e);
////        }
////
////        start = byteLength;
////        byteLength += 2;
////        // msgSn
////        String msgSnStr = StringUtils.substring(content, start, byteLength);
////        Integer msgSn;
////        try {
////            msgSn = Integer.valueOf(msgSnStr);
////        } catch (NumberFormatException e) {
////            e.printStackTrace();
////            // throw new IllegalArgumentException("msgSn解析失败", e);
////            throw new MessageResolverException(ErrorCode.err10005.getErrCode(), ErrorCode.err10005.getName(), e);
////        }
////
////        start = byteLength;
////        byteLength += 2;
////        // 设备id长度
////        int deviceIdLen = 0;
////        String deviceIdLenStr = StringUtils.substring(content, start, byteLength);
////        try {
////            deviceIdLen = Integer.valueOf(deviceIdLenStr);
////        } catch (NumberFormatException e) {
////            e.printStackTrace();
////            // throw new IllegalArgumentException("设备id长度解析失败", e);
////            throw new MessageResolverException(ErrorCode.err10006.getErrCode(), ErrorCode.err10006.getName(), e);
////        }
////        start = byteLength;
////        byteLength += deviceIdLen;
////        // 设备id
////        String deviceId = StringUtils.substring(content, start, byteLength);
////
////        start = byteLength;
////        byteLength += 1;
////        // 是否局域网
////        String isLanMsgStr = StringUtils.substring(content, start, byteLength);
////        Boolean isLanMsg;
////        try {
////            isLanMsg = Boolean.valueOf(isLanMsgStr);
////        } catch (Exception e) {
////            e.printStackTrace();
////            // throw new IllegalArgumentException("isLanMsg解析失败", e);
////            throw new MessageResolverException(ErrorCode.err10007.getErrCode(), ErrorCode.err10007.getName(), e);
////        }
////        start = byteLength;
////        String payload = StringUtils.substring(content, start);
////
////        NPiotTCPMessage npiotMessage = new NPiotTCPMessage();
////        npiotMessage.setDeviceId(deviceId);
////        npiotMessage.setIsLanMsg(isLanMsg);
////        npiotMessage.setMsgCode(msgCode);
////        npiotMessage.setMsgSn(msgSn);
////        npiotMessage.setPayload(payload);
////
////        return resolverByteToBean(npiotMessage);
        return null;
    }

//    private AbstractTCPMessage resolverByteToBean(NPiotTCPMessage npiotTCPMessage) {
//        Integer msgCode = npiotTCPMessage.getMsgCode();
//        MsgCode msgCode2 = MsgCode.getMsgCode(msgCode);
//        String payload = (String) npiotTCPMessage.getPayload();
//        if (null != msgCode2) {
//            Map<Class<?>, List<Field>> fieldListMap = msgCodeFieldListMap.get(msgCode2);
//            // 有msgCode并且能找到相应类，说明是对该msgCode定义过实体类，这种一般是平台内置的业务功能或者说内置的解析规则，则走实体类解析,否则走用户自定义解析规则
//            StringBuffer sb = new StringBuffer();
//            if (null != fieldListMap && !fieldListMap.isEmpty()) {
//                Set<Entry<Class<?>, List<Field>>> entrySet = fieldListMap.entrySet();
//                Object object = null;
//                for (Entry<Class<?>, List<Field>> entry : entrySet) {
//                    Class<?> clazz = entry.getKey();
//                    List<Field> fieldList = entry.getValue();
//                    int start = 0;
//                    int end = 0;
//                    int offset = 0;// 为了解决final问题，用数组
//                    for (Field field : fieldList) {
//                        TcpMessageLength tcpLength = field.getAnnotation(TcpMessageLength.class);
//                        int byteLength = tcpLength.value();
//                        end = end + offset + byteLength;
//                        String value = StringUtils.substring(payload, start, end);
//                        System.out.println("内置组件,解析出,propertiesName: " + field.getName() + ",value: " + value);
//                        // 如果检测到的负数,则要考虑-也要占一位，解析要整体向右偏移一个长度
//                        if (StringUtils.isNumeric(value)) {
//                            if (value.startsWith("-")) {
//                                offset++;
//                            }
//                        }
//                        start = end;
//                        Method setMethod = null;
//                        try {
//                            if (null == object) {
//                                object = clazz.newInstance();
//                            }
//                            setMethod = clazz.getMethod("set" + StringUtils.changFirstWord(field.getName(), StringUtils.toUpperCase), field.getType());
//                        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException e1) {
//                            e1.printStackTrace();
//                            throw new MessageResolverException(ErrorCode.err10008.getErrCode(), ErrorCode.err10010.getName() + ",字段: " + field.getName(), e1);
//                        }
//                        Invoker set = Invokers.newInvoker(setMethod);
//                        Class<?> type = field.getType();
//                        Object value2 = StringUtils.parseStr(value, type);
//                        System.out.println("转换成对应类型后: " + value2);
//                        set.invoke(object, new Object[]{value2});
//                    }
//                    NPiotTCPMessage npiotTCPMessage2 = (NPiotTCPMessage) SerializeUtil.deepClone(npiotTCPMessage);
//                    npiotTCPMessage2.setPayload(object);
//                    return npiotTCPMessage2;
//                }
//            }
//        }
//        // 如果不存在内置的msgCode,则按用户自定义的业务字段解析，解析成map,传送时转换成json,此时payload为String
//        return resolver(npiotTCPMessage);
//    }



    public abstract AbstractTCPMessage resolver(NPiotTCPMessage npiotMessage) throws MessageResolverException;
}
