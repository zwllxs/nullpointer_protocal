package ltd.nullpointer.tcp.core.message;

import java.io.Serializable;

/**
 * 订阅消息,主要是第三方平台的消息，不做包装
 * 
 * @Description:
 * @CreateDate:2018年4月16日 下午5:57:03
 * @Author:zhangweilin
 */
public class SubscriptionMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6641085486153192853L;

	/**
	 * 平台类型
	 */
	private String platformType;

	/**
	 * 第三方平台原始的消息
	 */
	private String message;

	public SubscriptionMessage(String platformType) {
		super();
		this.platformType = platformType;
	}

	public String getPlatformType() {
		return platformType;
	}

	public void setPlatformType(String platformType) {
		this.platformType = platformType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
