package ltd.nullpointer.tcp.core.resolver;

import lombok.extern.apachecommons.CommonsLog;
import ltd.nullpointer.tcp.core.message.NPiotTCPMessage;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ServerNPiotTCPMessageResolver.java
 * @Description: 平台业务消息
 * @author zhangweilin
 * @date 2018年1月21日 下午12:10:29
 *
 */
@Component
@CommonsLog
public class ServerNPiotTCPMessageResolver extends NPiotTCPMessageResolver {

//	@Autowired
//	DeviceJpaDao deviceJpaDao;
//
//	@Autowired
//	ProtocolItemJpaDao protocolItemJpaDao;
//
//	@Autowired
//	ProductJpaDao productJpaDao;
//
//	@Autowired
//	MqttProducer mqttProducer;

	@Override
	public NPiotTCPMessage resolver(NPiotTCPMessage npiotTCPMessage) {
//		String deviceId = npiotTCPMessage.getDeviceId();
//		Device device = deviceJpaDao.findByDeviceSn(deviceId);
//		String payload = (String) npiotTCPMessage.getPayload(); // 将报文解析成对象，只有可能是String类型
//		Product product = device.getProduct();
//		byte[] payloadByte = npiotTCPMessage.getPayloadByte();
//		System.out.println("准备解析payload: " + payload);
//		System.out.println("准备解析payloadByte: " + Arrays.toString(payloadByte));
//		if (null == device) {
//			throw new MessageResolverException(ErrorCode.err10014.getErrCode(), ErrorCode.err10014.getName() + ",deviceId: " + deviceId);
//		}
//
//		// key:属性名
//		final Map<String, ProtocolItem> protocolItemDataTypeMap = new HashMap<>();
//		final Map<String, Integer> protocolItemLengthMap = new HashMap<>();
//		List<ProtocolItem> protocolItemDataTypeList = protocolItemJpaDao.findByProductIdAndDataTypeIn(product.getId(), new int[] { 5 });
//		if (CollectionUtils.isNotEmpty(protocolItemDataTypeList)) {
//			for (ProtocolItem protocolItem : protocolItemDataTypeList) {
//				protocolItemDataTypeMap.put(protocolItem.getProductPropertiesCustomTag(), protocolItem); // 先只初始化key,value为对应的解析出来的长度
//			}
//		}
//
//		List<ProtocolItem> protocolItemList = protocolItemJpaDao.findByProductIdAndMsgCodeOrderBySort(product.getId(), npiotTCPMessage.getMsgCode());
//		int[] start = new int[1];
//		int[] end = new int[1];
//		int[] offset = new int[1];// 为了解决final问题，用数组
//		// // 要指定长度的字段
//		// Map<String, Integer> specifyLengthMap = new HashMap<>();
//		protocolItemList.stream().forEach(e -> {
//			String propertiesName = e.getProductPropertiesCustomTag();
//			// 数据类型
//			int type = e.getDataType();
//			int byteLength = e.getDataLength();
//			boolean isFixLength = e.getIsFixLength();
//
//			// 如果前面有指定长度类型，则取出之，如果有值，说明当前长度已经在报文中指定了类型同，此时则采用这种类型
//			// Integer length2 = specifyLengthMap.get(propertiesName);
//			// if (length2!=null) {
//			// byteLength=length2;
//			// }
//			// 如果为非定长字段
//			if (!isFixLength) {
//				if (protocolItemLengthMap.containsKey(propertiesName)) {
//					byteLength = protocolItemLengthMap.get(propertiesName);
//					System.out.println("检测到非定长字段，propertiesName: " + propertiesName + ",byteLength: " + byteLength);
//				}
//			}
//			end[0] = end[0] + offset[0] + byteLength;
//			String value = StringUtils.substring(payload, start[0], end[0]);
//			// 如果包含，说明当前字段为长度字段
//			if (protocolItemDataTypeMap.containsKey(propertiesName)) {
//				ProtocolItem protocolItem = protocolItemDataTypeMap.get(propertiesName);
//				protocolItemLengthMap.put(protocolItem.getLengthTarget(), Integer.valueOf(value));
//			}
//
//			// 6为要指定某字段的长度，如用户设置密码，密码长度不定，则要指定
//			// if (type == 6) {
//			// String targetProperty = e.getLengthTarget(); // 只能一个字段
//			// if (StringUtils.isEmpty(targetProperty)) {
//			// throw new MessageResolverException(ErrorCode.err10016, " deviceId: " +
//			// deviceId);
//			// }
//			// specifyLengthMap.put(targetProperty, Integer.valueOf(value));
//			// }
//
//			System.out.println("解析出,propertiesName: " + propertiesName + ",value: " + value);
//			// 如果检测到的负数,则要考虑-也要占一位，解析要整体向右偏移一个长度
//			if (StringUtils.isNumeric(value)) {
//				if (value.startsWith("-")) {
//					offset[0]++;
//				}
//			}
//			// 5为长度字段,不需要放到字段中，但是在序列化成字节时，则需要计算并拼到报文里去
//			if (type != 5) {
//				npiotTCPMessage.put(propertiesName, value);
//			}
//			start[0] = end[0];
//		});
//		npiotTCPMessage.put("deviceId", deviceId);
//		// TODO // 有可能mqtt的json中，消息体是dataMap而不是payload
//		// TODO 1：通过msgCode查找是否是内置协议，如果是，找到相应的实体，解析并塞回payload,
//
//		// =====以下为数据转发======
//		// Product product = productJpaDao.findOne(productId);
//		Service service = product.getService();
//
//		// 如果不为空，说明设置过转发规则
//		if (null != service) {
//			log.debug(" 找到转发规则，将【" + product.getName() + "】产品数据从设备【" + deviceId + "】转发到  " + service.getName());
//			String secretKey = service.getSecretKey();
//			String topic = TopicConstant.cdsTopic + secretKey;
//			System.out.println("topic: " + topic);
//			AbstractMQTTMessage<Map<String, Object>> mqttMessage = new AbstractMQTTMessage<>();
//			BeanUtils.copyProperties(npiotTCPMessage, mqttMessage);
//			mqttMessage.setPayload(npiotTCPMessage.getDataMap());
//			String json = JSON.toJSONString(mqttMessage);
//			System.out.println("json: " + json);
//			mqttProducer.send(topic, json);
//		}
//		System.out.println("服务端解析出最终npiotTCPMessage： " + npiotTCPMessage);
//		return npiotTCPMessage;
		return null;
	}

	/**
	 * 初始化基础信息
	 */
	@Override
	public void regHeader() {
		regHeader(NPiotTCPMessage.ARK_HEADER_BYTE);
	}

	@Override
	public Boolean isNeedEncrypt() {
		return false;
	}
}
