package ltd.nullpointer.tcp.core.message;

/**
 * 
 * @ClassName: BaseMessage.java
 * @Description: 抽象消息
 * @author zhangweilin
 * @date 2018年1月25日 下午3:44:40
 *
 */
public abstract class BaseMessage<T> extends AbstractNPiotMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -393331089467555441L;

	/**
	 * 是否是同步消息,即是否要回复
	 */
	private Boolean isSync;

	/**
	 * 原始的报文对应的字符串,字符串优先，即如果字符串有内容，则以它为数据体
	 */
	protected T payload;

	public T getPayload() {
		return payload;
	}

	public void setPayload(T payload) {
		this.payload = payload;
	}

	public Boolean getIsSync() {
		return isSync;
	}

	public void setIsSync(Boolean isSync) {
		this.isSync = isSync;
	}
}
