package ltd.nullpointer;

import com.boot2.core.utils.ByteUtils;
import ltd.nullpointer.tcp.core.util.BCDUtil;
import ltd.nullpointer.tcp.core.util.CHexConver;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/6
 */
public class Test1 {

    @Test
    public void test1() {
        byte[] bytes = BCDUtil.strToBcd("9");
        String str = new String(bytes);
        System.out.println("bytes = " + bytes);
        System.out.println("str = " + str);

        String str1 = CHexConver.byte2HexStr(bytes);
        System.out.println("str1 = " + str1);
    }

    @Test
    public void test2() throws DecoderException {
        //16进制字符串互转
        byte[] byteArr1 = new byte[]{(byte) 0xAA,(byte) 0x75,(byte) 0x6A,(byte) 0x00};
        String str1 = Hex.encodeHexString(byteArr1);
        System.out.println("16进制字符串互转,str1 = " + str1);
        byte[] byteArr2 = Hex.decodeHex(str1.toCharArray());
        System.out.println("16进制字符串互转,byteArr2 = " + byteArr2);
        String str2 = Hex.encodeHexString(byteArr2);
        System.out.println("16进制字符串互转,str2 = " + str2);

        //ascii码互转  59 48 30 30 32 30 20 20
        byte[] byteArr3 = new byte[]{(byte) 0x59,(byte) 0x48,(byte) 0x30,(byte) 0x30,(byte) 0x32,(byte) 0x30,(byte) 0x20,(byte) 0x20};
        String str3 = ByteUtils.getString(byteArr3, "GB2312");
        System.out.println("ascii码互转,str3 = " + str3);
        byte[] byteArr4 = ByteUtils.getBytes(str3, "GB2312");
        String str4 = ByteUtils.getString(byteArr4, "GB2312");
        System.out.println("ascii码互转,str4 = " + str4);

        //16进制字节到正负int 7F A4
        byte[] byteArr5 = new byte[]{(byte) 0xFF,(byte) 0xF0};
        Integer value = ByteUtils.hexByte2UnsignedInt(byteArr5);
        System.out.println("16进制字节到正负int,value = " + value);
        String byteArr5Hex = Integer.toHexString(value);
        System.out.println("byteArr5Hex = " + byteArr5Hex);

        String byteArr5Hex2 = Integer.toHexString(-1);
        System.out.println("16进制字节到正负int byteArr5Hex2 = " + byteArr5Hex2);
//        String byteArr5Hex3 = Integer.toHexString(-16 & 0xFF);
//        System.out.println("16进制字节到正负int byteArr5Hex3 = " + byteArr5Hex3);
//        String byteArr5Hex4 = Integer.toHexString(-1 & 0xFF);
//        System.out.println("16进制字节到正负int byteArr5Hex4 = " + byteArr5Hex4);
//        String byteArr5Hex5 = Integer.toHexString(55 & 0xFF);
//        System.out.println("16进制字节到正负int byteArr5Hex5 = " + byteArr5Hex5);
//        Integer value2 = ByteUtils.hexString2UnsignedInt(byteArr5Hex5);
//        System.out.println("16进制字节到正负int value2 = " + value2);
        String byteArr5Hex6 = ByteUtils.unsignedInt2HexString(value,2);
        System.out.println("16进制字节到正负int byteArr5Hex6 = " + byteArr5Hex6);
        String byteArr5Hex7 = ByteUtils.unsignedInt2HexString(26541,3);
//        String byteArr5Hex7 = Integer.toHexString(26541 & 0xFFFF);
//        String byteArr5Hex7 = Integer.toHexString(26541);
        System.out.println("16进制字节到正负int byteArr5Hex7 = " + byteArr5Hex7);
//        String byteArr5Hex8 = ByteUtils.unsignedInt2HexString(65192);
//        String byteArr5Hex8 = Integer.toHexString(65192 & 0xFFFF);
        String byteArr5Hex8 = Integer.toHexString(-65192);
        System.out.println("16进制字节到正负int byteArr5Hex8 = " + byteArr5Hex8);




        System.out.println("结束");
    }
}
