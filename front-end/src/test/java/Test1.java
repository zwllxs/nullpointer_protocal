import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import com.boot2.core.reflect.Invokers;
import com.boot2.core.utils.ReflectUtil;
import com.boot2.core.utils.StringUtils;
import javassist.*;
import javassist.Modifier;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.IntegerMemberValue;
import javassist.util.proxy.ProxyFactory;
import ltd.nullpointer.frontend.model.dto.up.SensorData1BDto;
import ltd.nullpointer.frontend.model.entity.up.SensorData;
import ltd.nullpointer.tcp.core.annotation.TcpUpOffset;
import ltd.nullpointer.tcp.core.constant.TCPEnum;
import ltd.nullpointer.tcp.core.exception.MessageResolverException;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.annotations.SQLDelete;
import org.junit.Test;

import java.lang.reflect.*;
import java.util.*;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/8
 */
public class Test1 {

    @Test
    public void test1() {
        List<Field> fieldList = ReflectUtil.getAllFieldListWithParentAndAnnotation(SensorData1BDto.class, TcpUpOffset.class, "MSG_CODE", "msgCode");
        for (int i = 0; i < fieldList.size(); i++) {
            Field field = fieldList.get(i);
            System.out.println("field = " + field);
            Class<?> type = field.getType();
            if (Collection.class.isAssignableFrom(type)) {
                System.out.println("   集合field = " + field + " ,type: " + type);
//                Type[] types = type.getGenericInterfaces();
//                System.out.println("types.byteLength = " + types.byteLength);
//                for (int j = 0; j < types.byteLength; j++) {
//                    ParameterizedType parameterizedType = (ParameterizedType) types[j];
//                    Type[] type2 = parameterizedType.getActualTypeArguments() ;
//                    for (int k = 0; k < type2.byteLength; k++) {
//                        System.out.println("集合field type2 = " + type2[k].getClass());
//                    }
//
//                }
                Type genericSuperclass = type.getGenericSuperclass();
                System.out.println("genericSuperclass = " + genericSuperclass);
                Class<?> clazz = (Class<?>) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
                System.out.println("clazz = " + clazz);
            }

        }
    }

    @Test
    public void testEval() {

        String expression = "a-(b-c)>100";
        Expression compiledExp = AviatorEvaluator.compile(expression);
        Map<String, Object> env = new HashMap<>();
        env.put("a", 100.3);
        env.put("b", 45);
        env.put("c", -199.100);
        Boolean result = (Boolean) compiledExp.execute(env);
        System.out.println(result);
    }

    @Test
    public void testEval2() {

        String expression = "this*0.3";
//        expression = expression.replace("this", "0.2");

        Expression compiledExp = AviatorEvaluator.compile(expression);
        Map<String, Object> env = new HashMap<>();
        env.put("this", 3);
        Object result = compiledExp.execute(env);
        System.out.println(result);
    }

    @Test
    public void testEval3() {

        String expression = "this*0.1M";
//        expression = expression.replace("this", "0.2");
        Expression compiledExp = AviatorEvaluator.compile(expression);
        Map<String, Object> env = new HashMap<>();
        env.put("this", 199);
        Object result = compiledExp.execute(env);
        System.out.println(result);
    }

    //https://blog.csdn.net/chenghaitao111111/article/details/78591336
    @Test
    public void testAddField() throws CannotCompileException, IllegalAccessException, InstantiationException, NotFoundException, ClassNotFoundException {
//        ClassPool pool = ClassPool.getDefault();
////        CtClass ctClass = pool.makeClass(SensorData1BDto.Data.class.getName());
////        CtClass ctClass = pool.makeClass(SensorData1BDto.class.getName()+"dd");
//        CtClass ctClass = pool.get("ltd.nullpointer.frontend.model.dto.SensorData1BDto");
////        CtClass ctClass = pool.get(SensorData1BDto.class.getName());
////        URL ctClass = pool.find(SensorData1BDto.class.getName());
////        Loader classLoader = new Loader(pool);
////        Class clazz = classLoader.loadClass(SensorData1BDto.class.getName());
////        CtClass ctClass = pool.get(SensorData1BDto.Data.class.getName());
//        CtField f1 = CtField.make("private Object obj;",ctClass);
//        ctClass.addField(f1);
//        Class<SensorData1BDto.Data>  clazz = ctClass.toClass();
//        Object instance = clazz.newInstance();
//        System.out.println("instance = " + instance);


        ClassPool pool = ClassPool.getDefault();
        Loader cl = new Loader(pool);
        CtClass ctClass = pool.get(SensorData1BDto.class.getName());
//        Class<?> c = cl.loadClass(SensorData1BDto.class.getName());
        CtField f1 = CtField.make("private Object obj;", ctClass);
        Class<?> c = cl.loadClass(SensorData1BDto.class.getName());
        Object rect = c.newInstance();
        System.out.println("rect = " + rect);
    }

    @Test
    public void testJavaassistAddField() throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException {
        ClassPool pool = ClassPool.getDefault();
        ProxyFactory factory = new ProxyFactory();
        // 设置父类，ProxyFactory将会动态生成一个类，继承该父类
        factory.setSuperclass(SensorData1BDto.class);
        Class aClass = factory.createClass();
        System.out.println("aClass = " + aClass);
        CtClass ctClass = pool.get(aClass.getName());
//        Class<?> c = cl.loadClass(SensorData1BDto.class.getName());
        CtField f1 = CtField.make("private Object obj;", ctClass);
        ctClass.addField(f1);
        Class clazz = ctClass.toClass();
        Object rect = clazz.newInstance();
        System.out.println("rect = " + rect);
    }


    @Test
    public void testJavaassistAddField2() throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException {
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass = pool.makeClass("test.SensorData1BDtoSuper1");
        CtField f1 = CtField.make("private Object obj;", ctClass);
        ctClass.addField(f1);
        Class aClass1 = ctClass.toClass();
        ProxyFactory factory = new ProxyFactory();
        // 设置父类，ProxyFactory将会动态生成一个类，继承该父类
        factory.setSuperclass(aClass1);
        Class aClass = factory.createClass();
        System.out.println("aClass = " + aClass);
    }

    @Test
    public void testJavaassistAddField3() throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchFieldException {
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass = pool.makeClass("test.SensorData1BDtoSuper1");
        CtField f1 = CtField.make("private Object obj;", ctClass);
        CtField f2 = CtField.make("private Object obj2;", ctClass);
        CtField f3 = CtField.make("private Object obj3;", ctClass);
        CtField f4 = CtField.make("private Object obj4;", ctClass);
        ctClass.addField(f1);
        ctClass.addField(f2);
        ctClass.addField(f3);
        ctClass.addField(f4);

        //5.创建方法
        CtMethod getter = CtMethod.make("public Object getObj() {return obj;}", ctClass);
        CtMethod setter = CtMethod.make("public void setObj(Object obj) {this.obj = obj;}", ctClass);

        ctClass.addMethod(getter);
        ctClass.addMethod(setter);

        // 为类型设置字段
        CtField field = new CtField(pool.get(String.class.getName()), "testObject", ctClass);
        field.setModifiers(Modifier.PRIVATE);
        // 添加getter和setter方法
        ctClass.addMethod(CtNewMethod.setter("setTestObject", field));
        ctClass.addMethod(CtNewMethod.getter("getTestObject", field));
        ctClass.addField(field);

        Class aClass = ctClass.toClass();
        Loader loader = new Loader(pool);

        CtClass ct = pool.get(SensorData1BDto.class.getName());
        ct.setSuperclass(pool.get("test.SensorData1BDtoSuper1"));
        Class c = loader.loadClass(SensorData1BDto.class.getName());

        Method[] declaredMethods = c.getDeclaredMethods();
        for (int i = 0; i < declaredMethods.length; i++) {
            Method declaredMethod = declaredMethods[i];
            System.out.println("declaredMethod = " + declaredMethod);
        }

        Object rect = c.newInstance();
        System.out.println("rect = " + rect);


        Field field2 = aClass.getDeclaredField("obj");
        ReflectUtil.makeFieldCanAccess(field2);
        System.out.println("field2 = " + field2);
        field2.set(rect, "23355能不能成");
        Object o = field2.get(rect);
        System.out.println("o = " + o);//能取到

        Method setMethod = null;
        String fieldName = "testObject";
        try {
            setMethod = aClass.getMethod("set" + StringUtils.changFirstWord(fieldName, StringUtils.toUpperCase), Object.class);
        } catch (NoSuchMethodException | SecurityException e1) {
            e1.printStackTrace();
            throw new MessageResolverException(TCPEnum.ErrorCode.err10010.getErrCode(), TCPEnum.ErrorCode.err10010.getName() + ",字段: " + fieldName, e1);
        }
        Invokers.Invoker setterInvoker = Invokers.newInvoker(setMethod);
        setterInvoker.invoke(rect, new Object[]{new Date()});

        System.out.println("赋值后");

        Method getMethod = null;
        try {
            getMethod = aClass.getMethod("get" + StringUtils.changFirstWord(fieldName, StringUtils.toUpperCase), Date.class);
        } catch (NoSuchMethodException | SecurityException e1) {
            e1.printStackTrace();
            throw new MessageResolverException(TCPEnum.ErrorCode.err10010.getErrCode(), TCPEnum.ErrorCode.err10010.getName() + ",字段: " + fieldName, e1);
        }
        Invokers.Invoker getterInvoker = Invokers.newInvoker(getMethod);
        Object value = getterInvoker.invoke(rect, new Object[]{});
        System.out.println("value = " + value);
    }

    @Test
    public void testJavaassistAddField4() throws ClassNotFoundException, CannotCompileException, InstantiationException, NotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        SensorData sensorData = ReflectUtil.newProxyInstance(SensorData.class);
        System.out.println("sensorData = " + sensorData);
        SQLDelete table = sensorData.getClass().getAnnotation(SQLDelete.class);
        System.out.println("table = " + table);
    }

    @Test
    public void testEnum() {
//        TcpUpMessage.CheckType checkType = TcpUpMessage.CheckType.BCC;
//        Class<? extends TcpUpMessage.CheckType> typeClass = checkType.getClass();
//        boolean anEnum = typeClass.isEnum();
//        System.out.println("anEnum = " + anEnum);
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass = pool.makeClass("test.SensorData1BDtoSuper1");
        ClassFile classFile = ctClass.getClassFile();
        ConstPool constPool = classFile.getConstPool();
        IntegerMemberValue integerMemberValue1 = new IntegerMemberValue(constPool);
        integerMemberValue1.setValue(333);
        System.out.println("integerMemberValue1 = " + integerMemberValue1.getValue());

        String str = "AA756A002C63151117170353677A6D303220202020202020202020200A28A7A59A01003D7701092FFC31342E31322E303201D3";
        System.out.println("str.byteLength() = " + str.length());
    }

    /**
     * 为class创建对象并动态追加属性
     *
     * @param clazz
     * @param fieldDefinition
     * @return
     */
//    public Object createAndAddField(Class<?> clazz,String... fieldDefinition) {
//
//    }
    @Test
    public void testNullMap() {
//        ImmutableMap<String, Object> immutableMap = ImmutableMap.of("stationNo", null);
//        System.out.println("immutableMap = " + immutableMap);

        Map<String,String> map=new HashMap<String,String>(1){{
            put("aaa", null);}};
        System.out.println("map = " + map);


        String str = "28F49900EA0400B2，28F4BA0111040030，28F48B020804002F";
        String[] sensorNoArr = str.split("[,，]");
        System.out.println("ArrayUtils.toString(sensorNoArr) = " + ArrayUtils.toString(sensorNoArr));

        List<Object> list = Arrays.asList(null);


    }

    @Test
    public void test2() {
        double a=0.1+0.2;
        System.out.println("a = " + a);
    }
}











