import com.boot2.core.utils.ByteUtils;
import com.boot2.core.utils.DateUtil;
import com.boot2.core.utils.RandomUtils;
import org.junit.Test;

import java.util.Date;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/15
 */
public class TestGenData {

    @Test
    public void testGenSensorData1() {
        //170302000156 00C7 7FA40000C23246B0
        String format = "yyMMddHHmmss";
        Date date = DateUtil.getFormatDate("170302000156", format);
        Date date2 = DateUtil.getFormatDate("170307000156", format);
        do {
            String dateStr = DateUtil.formatDate(date, format);
            Integer temp = RandomUtils.nextInt(0, 300);
            if (temp % 2 == 0) {
                temp = -temp;
            }
            String tempHexStr = ByteUtils.unsignedInt2HexString(temp.intValue(),2);
            System.out.println(dateStr + " " + tempHexStr.toUpperCase() + " 7FA40000C23246B0");
            date = DateUtil.addMin(date, 10);

        } while (date.getTime()<=date2.getTime());
    }
}
