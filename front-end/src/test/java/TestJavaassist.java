import com.google.common.collect.ImmutableMap;
import com.boot2.core.utils.BeanUtils;
import com.boot2.core.utils.ReflectUtil;
import com.boot2.core.utils.StringUtils;
import javassist.*;
import javassist.bytecode.AccessFlag;
import javassist.util.proxy.ProxyFactory;
import ltd.nullpointer.frontend.model.dto.up.SensorData1BDto;
import ltd.nullpointer.frontend.model.entity.up.SensorData;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/11
 */
public class TestJavaassist {

//    /**
//     * 使用原class创建代理类并增加一个属性
//     *
//     * @param clazz
//     * @param modifiers
//     * @param typeClassName
//     * @param fieldName0
//     * @return
//     * @throws NotFoundException
//     * @throws CannotCompileException
//     * @throws IllegalAccessException
//     * @throws InstantiationException
//     */
//    public static Object newInstanceAndAddField(Class<?> clazz, int modifiers, String typeClassName, String fieldName) throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException {
//        Field[] allFieldList = ReflectUtil.getAllFieldList(clazz);
//
//        //创建ClassPool
//        ClassPool cp = ClassPool.getDefault();
//        CtClass ctClass = cp.makeClass(clazz.getName() + "JavaassistProxy");
//        for (Field field : allFieldList) {
//
//            //创建字段，指定了字段类型、字段名称、字段所属的类
//            String fieldName0 = field.getName();
//            CtField ctField = new CtField(cp.get(field.getType().getName()), fieldName0, ctClass);
//            //指定该字段 修饰
//            ctField.setModifiers(field.getModifiers());
//            //设置prop字段的getter/setter方法
//            ctClass.addMethod(CtNewMethod.getter("get" + StringUtils.changFirstWord(fieldName0, StringUtils.toUpperCase), ctField));
//            ctClass.addMethod(CtNewMethod.setter("set" + StringUtils.changFirstWord(fieldName0, StringUtils.toUpperCase), ctField));
//
//            ctClass.addField(ctField);
//        }
//
//        CtField ctField = new CtField(cp.get(typeClassName), fieldName, ctClass);
//        //指定该字段 修饰
//        ctField.setModifiers(modifiers);
//        ctClass.addField(ctField);
//
//        //加载clazz类，并创建对象
//        Class<?> c = ctClass.toClass();
//        Object object = c.newInstance();
//        return object;
//    }
//
//    /**
//     * 使用反射设置变量值
//     *
//     * @param target 被调用对象
//     * @param fieldName 被调用对象的字段，一般是成员变量或静态变量，不可是常量！
//     * @param value 值
//     * @param <T> value类型，泛型
//     */
//    public static <T> void setFieldValue(Object target,String fieldName,T value) {
//        try {
//            Class<?> c = target.getClass();
//            Field f = c.getDeclaredField(fieldName);
//            f.setAccessible(true);
//            f.set(target, value);
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//            throw new RuntimeException("给field设置值失败",e);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//            throw new RuntimeException("给field设置值失败",e);
//        }
//    }
//
//    /**
//     * 使用反射获取变量值
//     *
//     * @param target 被调用对象
//     * @param fieldName 被调用对象的字段，一般是成员变量或静态变量，不可以是常量
//     * @param <T> 返回类型，泛型
//     * @return 值
//     */
//    public static <T> T getFieldValue(Object target,String fieldName) {
//        T value = null;
//        try {
//            Class<?> c = target.getClass();
//            Field f = c.getDeclaredField(fieldName);
//            f.setAccessible(true);
//            value = (T) f.get(target);
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//        return value;
//    }

//    /**
//     * 赋值
//     * @param fieldName
//     * @param value
//     */
//    public static void setValue( String fieldName, Object value) {
//        Field field = c.getDeclaredField("_map");
//        field.setAccessible(true);
//        field.set(object, ImmutableMap.of("age", 12, "sex", "男"));
//
//    }

    @Test
    public void test1() throws CannotCompileException, IllegalAccessException, InstantiationException, NotFoundException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        Class<SensorData> sensorDataClass = SensorData.class;
        Field[] allFieldList = ReflectUtil.getAllFieldList(sensorDataClass);

        //创建ClassPool
        ClassPool cp = ClassPool.getDefault();
        //生成的类的名称为com.guanjian.assist.JavassistTest
//        CtClass clazz = cp.makeClass(sensorDataClass.getName() + "Proxy");
        CtClass clazz = cp.makeClass(sensorDataClass.getName());
        for (Field field : allFieldList) {
            System.out.println("field = " + field);

            //创建字段，指定了字段类型、字段名称、字段所属的类
            String fieldName = field.getName();
            CtField ctField = new CtField(cp.get(field.getType().getName()), fieldName, clazz);
            //指定该字段 修饰
            ctField.setModifiers(field.getModifiers());
            //设置prop字段的getter/setter方法
            clazz.addMethod(CtNewMethod.getter("get" + StringUtils.changFirstWord(fieldName, StringUtils.toUpperCase), ctField));
            clazz.addMethod(CtNewMethod.setter("set" + StringUtils.changFirstWord(fieldName, StringUtils.toUpperCase), ctField));

            clazz.addField(ctField);
        }

        CtField ctField = new CtField(cp.get("java.util.Map"), "_map", clazz);
        //指定该字段 修饰
        ctField.setModifiers(AccessFlag.PRIVATE);
        clazz.addField(ctField);

        //加载clazz类，并创建对象
        Class<?> c = clazz.toClass();
        Object object = c.newInstance();
        System.out.println("object = " + object);


        //set
        Method setterMethod = c.getMethod("setSensorNo", String.class);
        setterMethod.invoke(object, "aaaaaaa4564756");

        //get
        Method getterMethod = c.getMethod("getSensorNo");
        Object invoke = getterMethod.invoke(object);
        System.out.println("invoke = " + invoke);

        Field field = c.getDeclaredField("_map");
        field.setAccessible(true);
        field.set(object, ImmutableMap.of("age", 12, "sex", "男"));
        Object o = field.get(object);
        System.out.println("o = " + o);

        SensorData sensorData = new SensorData();
        BeanUtils.copyProperties(object, sensorData);
        System.out.println("sensorData = " + sensorData);


//        if (clazz.isFrozen()) {
//            clazz.defrost();
//        }
//
//        CtField ctField = new CtField(cp.get("java.lang.Object"), "parent", clazz);
//        //指定该字段 修饰
//        ctField.setModifiers(AccessFlag.PRIVATE);
//        clazz.addField(ctField);
//        //加载clazz类，并创建对象
//        c = clazz.toClass();
//        object = c.newInstance();
//        System.out.println("object = " + object);
    }

    @Test
    public void test2() {
        try {
            Object o = ReflectUtil.newProxyInstanceAndAddField(SensorData.class, AccessFlag.PRIVATE, "java.util.Map", "_map");
            ReflectUtil.setFieldValue(o, "sensorNo", "测试序列号xxxxxx");
            ReflectUtil.setFieldValue(o, "sensorSelfNo", "yyyyyyyyyyyyy");
            System.out.println("o = " + o);
            System.out.println("(o instanceof SensorData) = " + (o instanceof SensorData));
//            SensorData sensorData = new SensorData();
//            BeanUtils.copyProperties(o, sensorData);
//            System.out.println("sensorData = " + sensorData);

        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (CannotCompileException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test3() throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException {
        ClassPool cp = ClassPool.getDefault();
        CtClass ctClass = cp.makeClass(SensorData.class.getName() + "JavaassistProxy11");
        System.out.println("ctClass = " + ctClass);
        ctClass = cp.makeClass(SensorData.class.getName() + "JavaassistProxy11");
        System.out.println("ctClass = " + ctClass);


        CtClass ctClass1 = null;
        try {
            ctClass1 = cp.get("dddd");
        } catch (NotFoundException e) {
            e.printStackTrace();
            ctClass1 = cp.makeClass("dddd");
        }
        System.out.println("ctClass1 = " + ctClass1);
        Object newInstance = ctClass1.toClass().newInstance();
        System.out.println("newInstance = " + newInstance);
    }


    @Test
    public void testJavaassistAddField() throws NotFoundException, CannotCompileException, IllegalAccessException, InstantiationException {
        ClassPool pool = ClassPool.getDefault();
        ProxyFactory factory = new ProxyFactory();
        // 设置父类，ProxyFactory将会动态生成一个类，继承该父类
        factory.setSuperclass(SensorData1BDto.class);
        Class aClass = factory.createClass();
        System.out.println("aClass = " + aClass);
        CtClass ctClass = pool.get(aClass.getName());
//        Class<?> c = cl.loadClass(SensorData1BDto.class.getName());
        CtField f1 = CtField.make("private Object obj;", ctClass);
        ctClass.addField(f1);
        Class clazz = ctClass.toClass();
        Object rect = clazz.newInstance();
        System.out.println("rect = " + rect);
    }

    @Test
    public void testJavaassist2() throws NotFoundException {
        ClassPool pool = ClassPool.getDefault();
        CtClass cc = pool.get(SensorData.class.getName());
        System.out.println("cc = " + cc);

        System.out.println("\"aaaa.bbb\".contains(\".\") = " + "aaaa.bbb".contains("."));
    }
}
