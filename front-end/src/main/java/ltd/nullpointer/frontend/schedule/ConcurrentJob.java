package ltd.nullpointer.frontend.schedule;

import com.boot2.core.scheduler.AbstractConcurrentJob;
import com.boot2.core.scheduler.ScheduleTaskHandler;
import lombok.extern.apachecommons.CommonsLog;

import java.util.Map;


/**
 * @author zhangweilin
 * @Description:并发job
 * @date 2018/6/7
 */
@CommonsLog
public class ConcurrentJob extends AbstractConcurrentJob {

    @Override
    public Map<String, ScheduleTaskHandler> getMap() {
        return SchedulerTaskRouter.map;
    }
}
