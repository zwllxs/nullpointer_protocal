package ltd.nullpointer.frontend.schedule;

import com.boot2.core.scheduler.AbstractUnConcurrentJob;
import com.boot2.core.scheduler.ScheduleTaskHandler;
import lombok.extern.apachecommons.CommonsLog;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.PersistJobDataAfterExecution;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description:
 * @date 2018/6/7
 */
@CommonsLog
@PersistJobDataAfterExecution
@DisallowConcurrentExecution  //此为禁止并发,是否支持并发，可配置在数据库中，当加载到是否并发时，通过反射动态创建此类
public class UnConcurrentJob extends AbstractUnConcurrentJob {

    @Override
    public Map<String, ScheduleTaskHandler> getMap() {
        return SchedulerTaskRouter.map;
    }
}
