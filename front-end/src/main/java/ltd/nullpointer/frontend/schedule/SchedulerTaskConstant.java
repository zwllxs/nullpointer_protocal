package ltd.nullpointer.frontend.schedule;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/20
 */
public class SchedulerTaskConstant {

    /**
     * 下发tcp消息
     */
    public static final String send_tcp_message = "send_tcp_message";

    /**
     * 传到定时任务中的类名
     */
    public static final String beanClassName ="beanClassName";

    /**
     * 传到定时任务中的参数json串
     */
    public static final String beanJsonValue = "beanJsonValue";


}
