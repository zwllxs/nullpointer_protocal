package ltd.nullpointer.frontend.schedule;

import com.boot2.core.scheduler.AbstractSchedulerTaskRouter;
import com.boot2.core.scheduler.ScheduleTaskHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
* @author zhangweilin
* @date 2018/7/4 17:30 
* @Description:  定时任务路由分发器
*/
@Component
public class SchedulerTaskRouter implements AbstractSchedulerTaskRouter {
    public static final Map<String, ScheduleTaskHandler> map = new HashMap<>();

    @Autowired
    SchedulerTaskSensorDataService schedulerTaskSensorDataService;

    /**
     * 各@PostConstruct之间顺序不好控制，故此处还是手动调用
     */
    @Override
    public void init()
    {
        SchedulerTaskRouter.map.put(SchedulerTaskConstant.send_tcp_message, schedulerTaskSensorDataService::sendTCPMessage);
    }
}
