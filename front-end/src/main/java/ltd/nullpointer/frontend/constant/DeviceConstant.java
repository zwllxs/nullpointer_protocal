package ltd.nullpointer.frontend.constant;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/18
 */
public class DeviceConstant {
    /**
     * 设备token映射列表，k: sensorNo, v:accessToken
     */
    public static final Map<String, String> deviceTokenMap = new ConcurrentHashMap<>();


    /**
     * 设备token映射列表，k: stationNo, v:sensorNo, 内存中通过sensorNo来维护长连接, ps: 由于一个模块可能有多个传感器，所以可能是多个sensorNo同时维护同一个长链接
     */
    public static final Map<String, List<String>> stationNoSensorNoListMap = new ConcurrentHashMap<>();


    public static final Set<String>  statonNoSensorNoCache = new CopyOnWriteArraySet<>();


}
