package ltd.nullpointer.frontend.web.conf;

import com.boot2.core.dao.i.MsTokenDao;
import com.boot2.core.web.AbstractMvcConfig;
import com.boot2.core.web.JwtAuthenticationFilter;
import com.boot2.core.web.SysInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

/**
 * Author wangzheng
 * Created on 2017/11/3.
 * Description
 */
@Configuration
public class WebMvcConfig extends AbstractMvcConfig {

    @Autowired
    SysInterceptor sysInterceptor;

    @Autowired
    MsTokenDao msTokenDao;

//
//    @Autowired
//    RedisDao redisDao;

    @Autowired
    RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    //添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        requestMappingHandlerAdapter.setIgnoreDefaultModelOnRedirect(true);
        registry.addInterceptor(sysInterceptor).addPathPatterns("/**"); // 系统级拦截，匹配全部
        super.addInterceptors(registry);
    }


    @Bean
    public FilterRegistrationBean userFilter() {
        String exludeUrlPattern = "/cacheMonitor/**,/user/sms/**,/swagger-ui.html,/webjars/**,/v2/api-docs/**,/swagger-resources/**,/user/login,/user/logout,/user/reg,/user/resetPassword";
        //此为无需要登录但是又要拿登录信息的
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter();
        jwtAuthenticationFilter.setProtectUrlPattern("/api/**");
        jwtAuthenticationFilter.setExludeUrlPattern(exludeUrlPattern == null ? "" : exludeUrlPattern);
//        jwtAuthenticationFilter.setRedisDao(redisDao);
        registrationBean.setFilter(jwtAuthenticationFilter);
        return registrationBean;
    }

}
