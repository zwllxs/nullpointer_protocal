package ltd.nullpointer.frontend.web.conf;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import com.boot2.core.conf.swagger.BaseSwagger2Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class Swagger2Config extends BaseSwagger2Config {

    

    @Bean
    public Docket userApi() {

//        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
//                .paths(PathSelectors.ant("/**/user")).build().groupName("用户中心").pathMapping("/")
//                .apiInfo(apiInfo());
//        List<Parameter> pars = getGlobalParameters("令牌,/user/**下的所有访问必填");
        String groupName = "用户端";
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        //声明swagger要以此域名为根路径,否则二级域名通过nginx转发后，还会以localhost为根路径导致404
//        docket = docket.host("http://api.v1.iyoju.com");
        docket.apiInfo(apiInfo(groupName)).forCodeGeneration(true).select()
                .apis(RequestHandlerSelectors.basePackage("ltd.nullpointer.frontend.web.controller.api")).paths(PathSelectors.any())
                .build().groupName(groupName).securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
          //要加上.globalOperationParameters(pars);才会在全局显示
        return docket;
    }


    private List<Parameter> getGlobalParameters(String description) {
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<Parameter>();
        tokenPar.name("x-access-token").description(description).modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());
        return pars;
    }

    private ApiInfo apiInfo(String groupName) {
        return new ApiInfoBuilder().title("NullPointer -" + groupName)// 大标题
                .description("NullPointer api-" + groupName)// 详细描述
                .termsOfServiceUrl("http://xxxx").version("1.0")
                .contact(new Contact("xxxx", "http://xxxxxxx.xx.xx", "xxx@xx.com"))// 作者
                .license("The Apache License, Version 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html").build();

    }

}