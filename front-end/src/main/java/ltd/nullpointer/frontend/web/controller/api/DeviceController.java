package ltd.nullpointer.frontend.web.controller.api;

import com.boot2.core.utils.DateUtil;
import com.boot2.core.vo.Response;
import io.netty.channel.Channel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.apachecommons.CommonsLog;
import ltd.nullpointer.frontend.dao.GprsModuleDao;
import ltd.nullpointer.frontend.dao.SensorDataDao;
import ltd.nullpointer.frontend.facade.SensorMessageDownFacade;
import ltd.nullpointer.frontend.model.entity.up.GprsModule;
import ltd.nullpointer.frontend.model.entity.up.SensorData;
import ltd.nullpointer.frontend.web.controller.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author zhangweilin
 * @date 2019/5/7 14:38
 * @Description: 设备
 */
@Api(tags = "设备")
@RestController
@RequestMapping("/user/device")
@CommonsLog
public class DeviceController extends ApiBaseController {

    @Autowired
    SensorMessageDownFacade sensorMessageDownFacade;

    @Autowired
    GprsModuleDao gprsModuleDao;

    @Autowired
    SensorDataDao sensorDataDao;

    /**
     * 打开自动采集箱总线电源 ,命令字 6CH
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @ApiOperation(value = "打开自动采集箱总线电源 ,命令字 6CH ")
    @GetMapping(value = "/turnOn/{sensorNo}")
    public Response<String> turnOn(@PathVariable String sensorNo) {
        sensorMessageDownFacade.handTurnOn(sensorNo);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 采集传感器编号,命令字 01H
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @ApiOperation(value = "采集传感器编号,命令字 01H ")
    @GetMapping(value = "/sensorNo/get/{sensorNo}")
    public Response<String> getSensorNo(@PathVariable String sensorNo, Date time) {
        sensorMessageDownFacade.handGetSensorNo(sensorNo, time);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 采集传感器数据,命令字 10H
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @ApiOperation(value = "采集传感器数据,命令字 10H ")
    @GetMapping(value = "/sensor/data/get/{sensorNo}")
    public Response<String> getSensorData(@PathVariable String sensorNo, Date time) {
        sensorMessageDownFacade.handGetSensorData(sensorNo, time);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 清除传感器中保存的数据记录,命令字 21H
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @ApiOperation(value = "清除传感器中保存的数据记录,命令字 21H ")
    @GetMapping(value = "/sensorNo/data/log/clear/{sensorNo}")
    public Response<String> clearSensorDataLog(@PathVariable String sensorNo, Date time) {
        sensorMessageDownFacade.handClearSensorDataLog(sensorNo, time);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 采集传感器中保存的数据记录,命令字 20H
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @ApiOperation(value = "采集传感器中保存的数据记录,命令字 20H")
    @GetMapping(value = "/sensorNo/data/log/get/{sensorNo}")
    public Response<String> getSensorDataLog(@PathVariable String sensorNo, Date time) {
        sensorMessageDownFacade.handGetSensorDataLog(sensorNo, time);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 传感器调零,命令字 80H
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @ApiOperation(value = "传感器调零,命令字 80H")
    @GetMapping(value = "/sensorNo/zero/set/{sensorNo}")
    public Response<String> setSensorZero(@PathVariable String sensorNo, Date time) {
        sensorMessageDownFacade.handSetSensorZero(sensorNo, time);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 读取指定日期雨量数据,命令字 1BH
     *
     * @param sensorNo
     * @param time1
     * @param time2
     * @return
     */
    @ApiOperation(value = "读取指定日期雨量数据,命令字 1BH ")
    @GetMapping(value = "/sensorNo/data/rain/get/{sensorNo}")
    public Response<String> getSensorRainData(@PathVariable String sensorNo, Date time1, Date time2) {
        sensorMessageDownFacade.handGetSensorRainData(sensorNo, time1, time2);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 设置传感器自设编号,命令字 82H
     *
     * @param sensorNo
     * @param selfNo
     * @param time
     * @return
     */
    @ApiOperation(value = "设置传感器自设编号,命令字 82H ")
    @GetMapping(value = "/sensorNo/selfNo/{sensorNo}")
    public Response<String> setSensorSelfNo(@PathVariable String sensorNo, String selfNo, Date time) {
        sensorMessageDownFacade.handSetSensorSelfNo(sensorNo, selfNo, time);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 采集传感器数据并保存至传感器 命令字 11H
     *
     * @return
     */
    @ApiOperation(value = "采集传感器数据并保存至传感器 命令字 11H")
    @GetMapping(value = "/sensor/data/store/{sensorNo}")
    public Response<String> getSensorDataAndStore(@PathVariable String sensorNo, Date time) {
        sensorMessageDownFacade.handGetSensorDataAndStore(sensorNo, time);
        return success("已向设备发送请求,请确保设备在线");
    }

    /**
     * 所有在线的传感器
     *
     * @return
     */
    @ApiOperation(value = "所有在线的传感器")
    @GetMapping(value = "/sensor/list/online")
    public Response<Map<String, String>> getOnlineDeviceList() {
        Map<String, Channel> onlineMap = getOnlineMap();
        Map<String, String> onlineMap2 = new HashMap<>();
        onlineMap.forEach((k, v) -> {
            onlineMap2.put(k, v.toString());
        });
        return success(onlineMap2);
    }

    @ApiOperation(value = "所有有心跳的传感器")
    @GetMapping(value = "/sensor/list/allModules")
    public Response<Set<String>> getGprsModules() {
        Set<String> moduleSet = new HashSet<>();
        List<GprsModule> gprsModules = gprsModuleDao.findAll();
        for(GprsModule module:gprsModules) {
            StringBuilder builder = new StringBuilder();
            builder.append(module.getIdentification());
            builder.append(" | ");
            builder.append(module.getInterval());
            builder.append(" | ");
            builder.append(module.getStationNo());
            builder.append(" | ");
            builder.append(module.getSensorNo());
            builder.append(" | ");
            builder.append(module.getTemp());
            builder.append(" | ");
            builder.append(module.getVoltage());
            moduleSet.add(builder.toString());
        }
        return success(moduleSet);
    }

    /**
     * 获取数据库存储的传感器数据
     *
     * @return
     */
    @ApiOperation(value = "获取数据库存储的传感器数据")
    @GetMapping(value = "/sensor/data/database/{sensorNo}")
    public Response<List<String>> getSensorDataFromDataBase(@PathVariable String sensorNo, String timeFrom, String timeTo) {
        List<String> result = new ArrayList<>();
        Date from = DateUtil.getDateByStr("1970-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
        if (timeFrom != null && !"".equals(timeFrom.trim())) from = DateUtil.getDateByStr(timeFrom, "yyyy-MM-dd HH:mm:ss");
        Date to = new Date();
        if (timeTo != null && !"".equals(timeTo.trim())) to = DateUtil.getDateByStr(timeTo, "yyyy-MM-dd HH:mm:ss");
        List<SensorData> datas = sensorDataDao.getSensorData(sensorNo, from, to);
        for(SensorData data: datas) {
            result.add(data.toString());
        }
        return success(result);
    }

}
