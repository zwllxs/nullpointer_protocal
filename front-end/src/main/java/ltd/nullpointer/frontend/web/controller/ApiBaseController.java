package ltd.nullpointer.frontend.web.controller;

import com.boot2.core.dao.PageInfo;
import com.boot2.core.web.BaseController;
import io.netty.channel.Channel;
import lombok.extern.apachecommons.CommonsLog;
import ltd.nullpointer.tcp.core.SessionManager;
import ltd.nullpointer.tcp.core.TCPMessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * @author zhangweilin
 * @Description: Controller基类
 * @date 2017年12月19日 下午5:12:47
 */
@CommonsLog
public class ApiBaseController extends BaseController {

    @Autowired
    SessionManager sessionManager;

    @Autowired
    TCPMessageProducer tcpMessageProducer;

    @Override
    protected void setCustomContext(Model model, HttpServletRequest request, HttpServletResponse response,
                                    PageInfo<?> pageInfo) {
//        addAttribute("indexPage", CoreUrlConstant.indexPage);
//        addAttribute("devUrl", CoreUrlConstant.devUrl);
//        addAttribute("resUrl", CoreUrlConstant.resUrl);
    }

    /**
     * 获取当前登录的用户id
     *
     * @return
     */
    protected Long getUserId() {
        return getJwtUserId();
    }


    public Map<String, Channel> getOnlineMap() {
        return sessionManager.getOnlineMap();
    }

    /**
     * 发送到设备
     *
     * @param deviceId
     * @param object
     */
    protected void sendToDevice(String deviceId, Object object) {
        tcpMessageProducer.sendToDevice(deviceId, object);
    }

}
