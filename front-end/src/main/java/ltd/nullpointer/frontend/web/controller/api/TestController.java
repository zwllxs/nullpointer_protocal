package ltd.nullpointer.frontend.web.controller.api;

import com.boot2.core.expiringmap.ExpiringMap;
import com.boot2.core.utils.StringUtils;
import com.boot2.core.vo.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.apachecommons.CommonsLog;
import ltd.nullpointer.frontend.service.NpAtrributeKvService;
import ltd.nullpointer.frontend.web.controller.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author zhangweilin
 * @date 2019/5/7 14:38
 * @Description: 设备
 */
@Api(tags = "test")
@RestController
@RequestMapping("/user/test")
@CommonsLog
public class TestController extends ApiBaseController {

    @Autowired
    NpAtrributeKvService npAtrributeKvService;

    /**
     * 打开自动采集箱总线电源 ,命令字 6CH
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @ApiOperation(value = "测试超时map")
    @GetMapping(value = "/testExpireMap/{key}/{value}")
    public Response<String> testExpireMap(@PathVariable String key, @PathVariable String value) {
        ExpiringMap<String, String> map = ExpiringMap.builder().expiration(10, TimeUnit.SECONDS).variableExpiration().expirationListener((k, v) -> {
            System.out.println("过期,k = " + k + " , v: " + v);
        }).build();
        map.put("a", "a");
        map.put("b", "b");
        map.put("c", "c");
        map.put("d", "d");
        map.setExpiration("c", 100, TimeUnit.MILLISECONDS);
        try {
            Thread.sleep(120);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("map.containsKey(\"a\") = " + map.containsKey("a"));
        System.out.println("map.containsKey(\"b\") = " + map.containsKey("b"));
        System.out.println("map.containsKey(\"c\") = " + map.containsKey("c"));
        System.out.println("map.containsKey(\"d\") = " + map.containsKey("d"));
        map.put(key, value);
        String value2 = map.get(key);
        System.out.println("value2 = " + value2);
        map.setExpiration("d",0, TimeUnit.MILLISECONDS);
        System.out.println("map.containsKey(\"d2\") = " + map.containsKey("d"));
        try {
            Thread.sleep(25 *1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("value22 = " + value2);

        return success(value2);
    }



}
