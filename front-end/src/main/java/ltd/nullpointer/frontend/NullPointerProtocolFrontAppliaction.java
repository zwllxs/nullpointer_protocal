package ltd.nullpointer.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author zhangweilin
 * @Description: 协议层入口
 * @date 2018年1月17日 下午2:01:06
 */
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan({"com.boot2", "ltd.nullpointer"})
@EnableJpaRepositories(value = {"com.boot2", "ltd.nullpointer"})
@EntityScan({"com.boot2", "ltd.nullpointer"})
//@RefreshScope  //动态刷新配置
@EnableCaching
@EnableAsync
//@SpringCloudApplication
public class NullPointerProtocolFrontAppliaction {
    public static void main(String[] args) {
        SpringApplication.run(NullPointerProtocolFrontAppliaction.class, args);
        System.out.println("启动完毕");
    }
}
