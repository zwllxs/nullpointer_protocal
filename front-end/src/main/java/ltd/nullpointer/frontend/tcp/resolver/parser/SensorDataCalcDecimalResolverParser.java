//package ltd.nullpointer.frontend.tcp.resolver.parser;
//
//import ltd.nullpointer.frontend.model.entity.up.SensorData;
//import ltd.nullpointer.tcp.core.resolver.parser.ResolverParser;
//import org.springframework.stereotype.Component;
//
//import java.math.BigDecimal;
//import java.util.Map;
//
///**
// * @author zhangweilin
// * @Description: 有两个特殊的字段，要从另外一个字段中读取精度，然后进行二次计算。如果没有特殊的二次计算，用不到自定义解析转换器
// * @date 2019/12/9
// */
//@Component
//@Deprecated //本转换器功能可以使用表达式来解决
//public class SensorDataCalcDecimalResolverParser implements ResolverParser<Void> {
//
//    /**
//     * @param byteArr
//     * @return
//     */
//    @Override
//    public Void resolve(byte[] byteArr, Object obj, Map<String, Object> paramMap) {
//        SensorData sensorData = (SensorData) obj;
//        Integer decimalFraction = sensorData.getType();
//
//        //精度调整 测量值
//        BigDecimal measureFloat = sensorData.getMeasurement().divide(new BigDecimal(Math.pow(10, decimalFraction)));
//        sensorData.setMeasurement(measureFloat);
//
//        //精度调整 偏移值
//        BigDecimal deviation = sensorData.getDeviation().divide(new BigDecimal(Math.pow(10, decimalFraction)));
//        sensorData.setDeviation(deviation);
//        return null;
//    }
//}
