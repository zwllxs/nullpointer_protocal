//package ltd.nullpointer.frontend.tcp.resolver;
//
//import io.netty.buffer.ByteBuf;
//import ltd.nullpointer.frontend.model.entity.up.SensorData;
//import ltd.nullpointer.tcp.core.message.NPiotTCPMessage;
//import org.springframework.stereotype.Component;
//
///**
// * @author zhangweilin
// * @Description: 具体的二次解析类,只有很复杂的，才建议往这走
// * @date 2019/12/7
// */
//@Component
//public class Message557AResolver {
//
//    public SensorData resolve10(ByteBuf byteBuf, NPiotTCPMessage npiotTCPMessage) {
//        SensorData sensorData = (SensorData) npiotTCPMessage.getPayload();
////        byteBuf.skipBytes(31);
////        //解析小数点位数
////        byte[] decimalFractionByte = new byte[1];
////        byteBuf.readBytes(decimalFractionByte);
////        int decimalFraction = ByteUtils.getHeight4(decimalFractionByte[0]);
////        System.out.println("decimalFraction = " + decimalFraction);
////        npiotTCPMessage.put("type", decimalFraction);  //???不明白为啥叫类型
////        sensorData.setType(decimalFraction);
//
////        //精度调整 测量值
////        BigDecimal measureFloat = sensorData.getMeasurement().divide(new BigDecimal(Math.pow(10, decimalFraction)));
////        sensorData.setMeasurement(measureFloat);
////
////        //精度调整 偏移值
////        BigDecimal deviation = sensorData.getDeviation().divide(new BigDecimal(Math.pow(10, decimalFraction)));
////        sensorData.setDeviation(deviation);
//
//        return sensorData;
//    }
//}
