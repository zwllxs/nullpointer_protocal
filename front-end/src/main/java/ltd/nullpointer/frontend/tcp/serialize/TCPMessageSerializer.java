package ltd.nullpointer.frontend.tcp.serialize;

import ltd.nullpointer.frontend.facade.TCPComponentsFacade;
import ltd.nullpointer.tcp.core.serialize.AbstractTCPMessageSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhangweilin
 * @Description: 序列化
 * @date 2019/12/16
 */
@Component
public class TCPMessageSerializer extends AbstractTCPMessageSerializer {


    @Autowired
    TCPComponentsFacade tcpComponentsFacade;

    @Override
    protected void beforeSerialize(Object payload) {
        tcpComponentsFacade.save(payload);
    }
}
