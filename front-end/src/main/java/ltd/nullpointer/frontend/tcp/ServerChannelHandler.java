package ltd.nullpointer.frontend.tcp;

import com.boot2.core.utils.BeanUtils;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.apachecommons.CommonsLog;
import ltd.nullpointer.frontend.facade.SensorMessageUpFacade;
import ltd.nullpointer.frontend.facade.TCPComponentsFacade;
import ltd.nullpointer.frontend.model.dto.up.SensorData1BDto;
import ltd.nullpointer.frontend.model.entity.down.ModuleHeartBeatDown;
import ltd.nullpointer.frontend.model.entity.up.*;
import ltd.nullpointer.frontend.service.NpAtrributeKvService;
import ltd.nullpointer.tcp.core.message.NPiotTCPMessage;
import ltd.nullpointer.tcp.core.message.TcpMessageAdaptor;
import ltd.nullpointer.tcp.server.netty.AbstractServerChannelHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author zhangweilin
 * @Description: TODO
 * @date 2017年12月7日 下午2:17:08
 */
@Component
@ChannelHandler.Sharable
@CommonsLog
public class ServerChannelHandler extends AbstractServerChannelHandler {
//
//    @Autowired
//    SensorDataDao sensorDataDao;

    @Autowired
    SensorMessageUpFacade sensorFacade;

    @Autowired
    TCPComponentsFacade tcpComponentsFacade;

    @Autowired
    NpAtrributeKvService npAtrributeKvService;

//    @Autowired
//    TCPMessageProducer tcpMessageProducer;

    @Override
    protected void onMessage(ChannelHandlerContext ctx2, NPiotTCPMessage npiotTCPMessage) {
        Object payload = npiotTCPMessage.getPayload();
//        tcpComponentsFacade.save(payload);
//        tcpMessageProducer.handReply(payload);

        if (payload instanceof SensorData) {
            SensorData sensorData = (SensorData) payload;
            onMessage(ctx2, sensorData);
        } else if (payload instanceof SensorData1BDto) {
            SensorData1BDto sensorData = (SensorData1BDto) payload;
            onMessage(ctx2, sensorData);
        } else if (payload instanceof ModuleHeartBeatUp) {
            ModuleHeartBeatUp moduleHeartBeat = (ModuleHeartBeatUp) payload;
            onMessage(ctx2, moduleHeartBeat);
        } else if (payload instanceof SensorNo) {
            SensorNo sensorNo = (SensorNo) payload;
            onMessage(ctx2, sensorNo);
        } else if (payload instanceof SensorClearDataLog) {
            SensorClearDataLog sensorClearDataLog = (SensorClearDataLog) payload;
            onMessage(ctx2, sensorClearDataLog);
        } else if (payload instanceof SensorSetSelfNo) {
            SensorSetSelfNo sensorSetSelfNo = (SensorSetSelfNo) payload;
            onMessage(ctx2, sensorSetSelfNo);
        } else if (payload instanceof SensorZero) {
            SensorZero sensorZero = (SensorZero) payload;
            onMessage(ctx2, sensorZero);
        }
    }

    private void onMessage(ChannelHandlerContext ctx2, SensorZero sensorZero) {
    }

    private void onMessage(ChannelHandlerContext ctx2, SensorSetSelfNo sensorSetSelfNo) {
    }

    private void onMessage(ChannelHandlerContext ctx2, SensorClearDataLog sensorClearDataLog) {
    }

    private void onMessage(ChannelHandlerContext ctx2, SensorNo sensorNo) {


    }

    protected void onMessage(ChannelHandlerContext ctx2, ModuleHeartBeatUp moduleHeartBeat) {
        //此实际为模块编号
        String stationNo = moduleHeartBeat.getSensorNo();
        List<String> sensorNoList = npAtrributeKvService.getSensorNoListByStationNo(stationNo);
        sensorFacade.handleGprsModuleData(moduleHeartBeat, sensorNoList);
        Assert.notEmpty(sensorNoList, "该通讯模块没有关联传感器,模块编号:" + stationNo);
        if (CollectionUtils.isNotEmpty(sensorNoList)) {
            for (String sensorNo0 : sensorNoList) {
                //由于业务操作都是基于传感器编号，通过传感器编号来维护长链接
                sessionManager.addOrUpdateChannel(sensorNo0, ctx2.channel());
            }
        }
        TcpMessageAdaptor<ModuleHeartBeatDown> nPiotTCPMessage = new TcpMessageAdaptor<>();
        ModuleHeartBeatDown moduleHeartBeatDown = new ModuleHeartBeatDown();
        moduleHeartBeatDown.setCreateTime();
        nPiotTCPMessage.setPayload(moduleHeartBeatDown);
        ctx2.writeAndFlush(nPiotTCPMessage);
    }

    protected void onMessage(ChannelHandlerContext ctx2, SensorData1BDto sensorData1BDto) {
        sensorFacade.handMultiSensorData(sensorData1BDto);


//        List<SensorData1BDto.Data> dataDataList = sensorData.getDataList();
//        for (int i = 0; i < dataDataList.size(); i++) {
//            SensorData1BDto.Data data = dataDataList.get(i);
//            System.out.println("data = " + data);
//        }

    }

    protected void onMessage(ChannelHandlerContext ctx2, SensorData sensorData) {
        System.out.println("sensorData = " + sensorData);
        sensorFacade.handSensorData(sensorData);

//        System.out.println("sensorDataDao = " + sensorDataDao);
//        String url = "http://localhost:8080/api/v1/lpIbE3mNJT1nKIbTDSfb/telemetry";
//        String json = JSONObject.toJSONString(sensorData);
////        System.out.println("json = " + json);
//        ;
//        threadPoolTaskExecutorUtil.submit(() -> {
//            HttpsClient.postJson(url, json);
//        });

//        sensorDataDao.saveAndFlush(sensorData);
    }
}
