package ltd.nullpointer.frontend.tcp;

import io.netty.channel.Channel;
import ltd.nullpointer.frontend.model.entity.down.TurnOff;
import ltd.nullpointer.frontend.model.entity.down.TurnOn;
import ltd.nullpointer.tcp.core.DefaultTcpMessageProducer;
import ltd.nullpointer.tcp.core.message.TcpMessageAdaptor;
import org.springframework.stereotype.Service;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/22
 */
@Service
public class NullPointerTcpMessageProducer extends DefaultTcpMessageProducer {

    /**
     * 在给传感器发送指令之前，先要打开无线模块的总线电源，也就是
     * 4，发送55 7A 6C开头的指令给无线模块打开总线电源，等待至少3秒的时间再发送其他指令
     * 5，发送传感器的采集指令.....
     * 6,处理完所有传感器的指令之后发送55 7A 6D开头的指令关闭无线模块上的总线电源
     *
     * 此为第4第5步
     * @param channel
     * @param object
     */
    @Override
    public void turnOn(Channel channel, Object object) {
        //如果传过来的就是上电操作，则忽略
        if (object.getClass().getName().equalsIgnoreCase(TurnOn.class.getName())) {
            return;
        }
        //执行上电操作
        TurnOn turnOn = new TurnOn();
        TcpMessageAdaptor<?> nPiotTCPMessage = new TcpMessageAdaptor<>(turnOn);
        channel.writeAndFlush(nPiotTCPMessage);
        //上电后要等待至少3s
        try {
            Thread.sleep(3500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 在给传感器发送指令之前，先要打开无线模块的总线电源，也就是
     * 4，发送55 7A 6C开头的指令给无线模块打开总线电源，等待至少3秒的时间再发送其他指令
     * 5，发送传感器的采集指令.....
     * 6,处理完所有传感器的指令之后发送55 7A 6D开头的指令关闭无线模块上的总线电源
     *
     * 这是第6步
     * @param channel
     */
    @Override
    public void turnOff(Channel channel) {
        TurnOff turnOff = new TurnOff();
        TcpMessageAdaptor<?> nPiotTCPMessage = new TcpMessageAdaptor<>(turnOff);
        channel.writeAndFlush(nPiotTCPMessage);
    }
}
