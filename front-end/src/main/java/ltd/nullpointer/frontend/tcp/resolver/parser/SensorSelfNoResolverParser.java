package ltd.nullpointer.frontend.tcp.resolver.parser;

import com.boot2.core.utils.ByteUtils;
import ltd.nullpointer.tcp.core.resolver.parser.ResolverParser;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangweilin
 * @Description: 由于自编号可能有空，即FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF，按ASCII码转出来是乱码，故要特殊处理
 * @date 2019/12/9
 */
@Component
public class SensorSelfNoResolverParser implements ResolverParser<String> {

    /**
     * @param byteArr
     * @return
     */
    @Override
    public String resolve(byte[] byteArr,Object obj, Map<String, Object> paramMap) {
        String hex = ByteUtils.hexBytes2HexString(byteArr);
        if ("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF".equalsIgnoreCase(hex)) {
            return "";
        }
        return hex;
    }
}
