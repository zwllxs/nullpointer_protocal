//package ltd.nullpointer.frontend.message;
//
//import ltd.nullpointer.tcp.core.message.TcpMessageAdaptor;
//import org.apache.commons.lang3.ArrayUtils;
//
///**
// * @Description:
// * @CreateDate:2018年4月26日 下午2:06:32
// * @Author:zhangweilin
// */
//public class Message557A extends TcpMessageAdaptor<String> {
//
//    public static final byte[] BEGIN = new byte[]{(byte) 0x55, (byte) 0x7A};
//    /**
//     *
//     */
//    private static final long serialVersionUID = -7103000943243851557L;
////	public static final byte[] END = new byte[] { (byte) 0xF0, (byte) 0xC3, (byte) 0x3C, (byte) 0x0F };
//
//    @Override
//    public byte[] getHeader() {
//        if (ArrayUtils.isEmpty(header)) {
//            header = BEGIN;
//        }
//        return header;
//    }
//
////	@Override
////	public byte[] getTail() {
////		if (ArrayUtils.isEmpty(tail)) {
////			tail = END;
////		}
////		return tail;
////	}
//
////    public enum MsgType {
////        /**
////         *
////         */
////        MsgType01((byte) 0x01, "采集传感器编号自编号及注释信息"),
////        MsgType10((byte) 0x10, "采集传感器数据"),
////        MsgType11((byte) 0x11, "采集传感器数据并保存到传感器中"),
////        MsgType1B((byte) 0x1B, "读取指定日期内存数据"),
////        MsgType20((byte) 0x20, "采集传感器中保存的数据记录"),
////        MsgType21((byte) 0x21, "清除传感器中保存的数据记录"),
////        MsgType80((byte) 0x80, "传感器调零"),
////        MsgType82((byte) 0x82, "设置传感器自设编号");
////
////        private byte byteKey;
////        private String description;
////        MsgType(byte byteKey, String description) {
////            this.byteKey = byteKey;
////            this.description = description;
////        }
////
////        public byte getByteKey() {
////            return byteKey;
////        }
////
////        public void setByteKey(byte byteKey) {
////            this.byteKey = byteKey;
////        }
////
////        public String getDescription() {
////            return description;
////        }
////
////        public void setDescription(String description) {
////            this.description = description;
////        }
////    }
//}
