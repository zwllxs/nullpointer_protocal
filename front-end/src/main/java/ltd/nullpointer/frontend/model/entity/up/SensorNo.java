package ltd.nullpointer.frontend.model.entity.up;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import ltd.nullpointer.tcp.core.annotation.DeviceSn;
import ltd.nullpointer.tcp.core.annotation.TcpUpMessage;
import ltd.nullpointer.tcp.core.annotation.TcpUpOffset;
import ltd.nullpointer.tcp.core.constant.TCPEnum;
import ltd.nullpointer.tcp.core.resolver.parser.Height4ToIntResolverParser;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author zhangweilin
 * @Description: 传感器编号
 * @date 2019/12/7
 */
@ApiModel("传感器编号")
@TcpUpMessage(start = 7,messageCode="557A01")
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_sensor_no set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class SensorNo extends LongBaseEntity {

    /**
     * 传感器编号
     */
    @ApiModelProperty("传感器编号")
    @TcpUpOffset(start = 1, end = 8)
    @DeviceSn
    private String sensorNo;

    /**
     * 传感器型号
     */
    @ApiModelProperty("传感器型号")
    @TcpUpOffset(start = 9, end = 16, resolveType = TCPEnum.ResolveType.ascii)
    private String model;


    /**
     * 生产日期
     */
    @ApiModelProperty("生产日期")
    @TcpUpOffset(start = 17, end = 19, dateFormat = "yyMMdd")
    private Date produceDate;


    /**
     * 传感器类型(小数点位)
     */
    @ApiModelProperty("传感器类型(小数点位),取高四位")
//    @TcpUpOffset(start = 29, end = 29, resolverParser = {Height4ToIntResolverParser.class, SensorDataCalcDecimalResolverParser.class})
    @TcpUpOffset(start = 20, end = 20, resolverParser = Height4ToIntResolverParser.class)
    private Integer type;

    /**
     * 温度补偿系数（ppm 负数用补码）
     */
    @ApiModelProperty("温度补偿系数（ppm 负数用补码）")
//    @TcpUpOffset(start = 29, end = 29, resolverParser = {Height4ToIntResolverParser.class, SensorDataCalcDecimalResolverParser.class})
    @TcpUpOffset(start = 21, end = 22, resolveType = TCPEnum.ResolveType.hexByte2UnsignedInt )
    private Integer tempCompensate;


    /**
     * 传感器补偿频率
     */
    @ApiModelProperty("调零点频率(Hz)")
    @TcpUpOffset(start = 23, end = 24, resolveType = TCPEnum.ResolveType.hexByte2Int)
    private Integer zeroFre;


    /**
     * 传感器应变单位
     */
    @ApiModelProperty("传感器应变单位")
    @TcpUpOffset(start = 25, end = 28, resolveType = TCPEnum.ResolveType.ascii)
    private String unit;


    /**
     * 传感器自动测量时间（分钟）
     */
    @ApiModelProperty("传感器自动测量时间（分钟）")
    @TcpUpOffset(start = 29, end = 30, resolveType = TCPEnum.ResolveType.hexByte2Int)
    private Integer strainFre;


    /**
     * 自动测量设置日期（年月日）
     */
    @ApiModelProperty("自动测量设置日期（年月日）")
    @TcpUpOffset(start = 31, end = 33, dateFormat = "yyMMdd")
    private Date measuringDate;


    /**
     * 自编号设置日期（年月日时分秒）
     */
    @ApiModelProperty("自编号设置日期（年月日时分秒）")
    @TcpUpOffset(start = 34, end = 39, dateFormat = "yyMMddhhmmss")
    private Date sensorSelfNoDate;

    /**
     * 传感器自编号
     */
    @ApiModelProperty("传感器自编号")
    @TcpUpOffset(start = 40, end = 55, resolveType = TCPEnum.ResolveType.ascii)
    private String sensorSelfNo;


    /**
     * 注释信息（106 字符或 53 个汉字）
     */
    @ApiModelProperty("注释信息（106 字符或 53 个汉字）")
    @TcpUpOffset(start = 56, end = 161, resolveType = TCPEnum.ResolveType.ascii)
    private String remark;

    /**
     * 传感器内可以保存的数据个数
     */
    @ApiModelProperty("传感器内可以保存的数据个数")
    @TcpUpOffset(start = 162, end = 163, resolveType = TCPEnum.ResolveType.hexByte2Int)
    private Integer dataCapacity;
}