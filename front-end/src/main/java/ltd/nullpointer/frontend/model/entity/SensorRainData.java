package ltd.nullpointer.frontend.model.entity;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author zhangweilin
 * @Description: 传感器数据
 * @date 2019/12/7
 */
@ApiModel("传感器雨量数据")
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_sensor_rain_data set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class SensorRainData extends LongBaseEntity {

    /**
     * 记录个数
     */
    @ApiModelProperty(value = "记录个数")
    private Integer recordNum;

    /**
     * 传感器编号
     */
    @ApiModelProperty(value = "传感器编号")
    private String sensorNo;

    /**
     * 传感器应变单位
     */
    @ApiModelProperty(value = "传感器应变单位")
    private String unit;


    /**
     * 传感器类型(小数点位)
     */
    @ApiModelProperty(value = "传感器类型(小数点位)")
    private Integer type;


    /**
     * 传感器自编号
     */
    @ApiModelProperty(value = "传感器自编号")
    private String sensorSelfNo;

    /**
     * 传感器型号
     */
    @ApiModelProperty(value = "传感器型号")
    private String model;

}