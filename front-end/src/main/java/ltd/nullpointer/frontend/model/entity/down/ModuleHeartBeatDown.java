package ltd.nullpointer.frontend.model.entity.down;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;
import ltd.nullpointer.tcp.core.annotation.TcpDownMessage;
import ltd.nullpointer.tcp.core.annotation.TcpHeader;
import ltd.nullpointer.tcp.core.annotation.TcpType;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author zhangweilin
 * @Description: 模块通讯下行心跳
 * @date 2019/12/7
 */
@ApiModel("模块通讯下行心跳")
@TcpDownMessage(header = @TcpHeader("557A"), type = @TcpType("6A"),isPersistence = false)
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_module_heart_beat_down set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class ModuleHeartBeatDown extends LongBaseEntity {

}