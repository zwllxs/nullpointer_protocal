package ltd.nullpointer.frontend.model.entity;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author zhangweilin
 * @Description: 需要定时调度下发的tcp消息
 * @date 2019/12/7
 */
@ApiModel("需要定时调度下发的tcp消息")
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_scheduler_task_to_sendtcpmessage set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class SchedulerTaskToSendTCPMessage extends LongBaseEntity {

    /**
     * 传感器编号
     */
    @ApiModelProperty(value = "传感器编号")
    private String sensorNo;

    /**
     * 任务的时间表达式
     */
    @ApiModelProperty("任务的时间表达式")
    private String cronExpression;

//    /**
//     * 设备类别，和代码中SnTypeEnum关联
//     */
//    @ApiModelProperty(value = "设备类别，和代码中SnTypeEnum关联")
//    private String category;

    /**
     * 关联的参数，在此存储下发的控制参数
     */
    @ApiModelProperty(value = "关联的参数，在此存储下发的控制参数。此格式由入库和读取约定解析")
    private String paramJson;

    /**
     * 下一次执行的时间
     */
    @ApiModelProperty(value = "下一次执行的时间")
    private Date nextFireTime;

    /**
     * 如果是一次性调度任务，执行完后是否删除
     */
    @ApiModelProperty(value = "如果是一次性调度任务，执行完后是否删除")
    private Boolean isDeleteOneTime;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


    public SchedulerTaskToSendTCPMessage() {
    }

    public SchedulerTaskToSendTCPMessage(boolean isInit) {
        super(isInit);
    }
}