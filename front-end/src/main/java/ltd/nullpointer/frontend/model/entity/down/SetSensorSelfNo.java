package ltd.nullpointer.frontend.model.entity.down;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import ltd.nullpointer.tcp.core.annotation.*;
import ltd.nullpointer.tcp.core.constant.TCPEnum;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author zhangweilin
 * @Description: 设置传感器自设编号
 * @date 2019/12/7
 */
@ApiModel("设置传感器自设编号")
@TcpDownMessage(header = @TcpHeader("AA75"), replyType = "557A82", type = @TcpType("82"))
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_set_sensor_self_no set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class SetSensorSelfNo extends LongBaseEntity {

    /**
     * 传感器编号
     */
    @ApiModelProperty("传感器编号")
    @TcpDownOffset(start = 1, end = 8)
    private String sensorNo;

    /**
     * 通信机日期时间（年月日时分秒）
     */
    @ApiModelProperty("通信机日期时间（年月日时分秒）")
    @TcpDownOffset(start = 9, end = 14, dateFormat = "yyMMddHHmmss")
    private Date time=new Date();

    /**
     * 传感器自编号
     */
    @ApiModelProperty("传感器自编号")
    @TcpUpOffset(start = 15, end = 31, resolveType = TCPEnum.ResolveType.ascii)
    private String sensorSelfNo;

    public SetSensorSelfNo(String sensorNo, Date time, String sensorSelfNo) {
        this.sensorNo = sensorNo;
        this.time = time;
        this.sensorSelfNo = sensorSelfNo;
    }

    public SetSensorSelfNo(boolean isInit, String sensorNo, Date time, String sensorSelfNo) {
        super(isInit);
        this.sensorNo = sensorNo;
        this.time = time;
        this.sensorSelfNo = sensorSelfNo;
    }
}