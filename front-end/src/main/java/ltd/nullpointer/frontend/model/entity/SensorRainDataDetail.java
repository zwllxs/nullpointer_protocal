package ltd.nullpointer.frontend.model.entity;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zhangweilin
 * @Description: 传感器数据
 * @date 2019/12/7
 */
@ApiModel("传感器雨量数据明细")
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_sensor_rain_data_detail set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class SensorRainDataDetail extends LongBaseEntity {

    /**
     * 关联的传感器
     */
    @ApiModelProperty(value = "关联的传感器")
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "sensor_rain_data_id", nullable = true)
    @NotFound(action = NotFoundAction.IGNORE)
    private SensorRainData sensorRainData;

    /**
     * 传感器编号
     */
    @ApiModelProperty(value = "传感器编号")
    private String sensorNo;


    /**
     * 测量时间
     */
    @ApiModelProperty(value = "测量时间")
    private Date measuringDate;

    /**
     * 温度
     */
    @ApiModelProperty(value = "温度")
    private BigDecimal temp;

    /**
     * 测量值
     */
    @ApiModelProperty(value = "测量值(修正后的)")
    private BigDecimal measurement;

    /**
     * 测量值
     */
    @ApiModelProperty(value = "测量值(原始的)")
    private BigDecimal originalMeasure;

    /**
     * 偏移值
     */
    @ApiModelProperty(value = "偏移值")
    private BigDecimal deviation;

    /**
     * 传感器应变频率
     */
    @ApiModelProperty(value = "传感器应变频率")
    private Integer strainFre;

    /**
     * 传感器补偿频率
     */
    @ApiModelProperty(value = "传感器补偿频率")
    private Integer comFre;

    /**
     * 测量时间-年
     */
    @ApiModelProperty(value = "测量时间-年")
    private Integer year;

    /**
     * 测量时间-月
     */
    @ApiModelProperty(value = "测量时间-月")
    private Integer month;

    /**
     * 测量时间-日
     */
    @ApiModelProperty(value = "测量时间-日")
    private Integer day;

    /**
     * 测量时间-时
     */
    @ApiModelProperty(value = "测量时间-时")
    private Integer hour;

    /**
     * 测量时间-分
     */
    @ApiModelProperty(value = "测量时间-分")
    private Integer min;

}