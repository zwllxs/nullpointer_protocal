package ltd.nullpointer.frontend.model.entity.down;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import ltd.nullpointer.tcp.core.annotation.TcpDownMessage;
import ltd.nullpointer.tcp.core.annotation.TcpDownOffset;
import ltd.nullpointer.tcp.core.annotation.TcpHeader;
import ltd.nullpointer.tcp.core.annotation.TcpType;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author zhangweilin
 * @Description: 读取指定日期雨量数据
 * @date 2019/12/7
 */
@ApiModel("读取指定日期雨量数据")
@TcpDownMessage(header = @TcpHeader("AA75"), replyType = "557A20", type = @TcpType("1B"))
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_get_sensor_rain_data set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class GetSensorRainData extends LongBaseEntity {

    /**
     * 传感器编号
     */
    @ApiModelProperty("传感器编号")
    @TcpDownOffset(start = 1, end = 8)
    private String sensorNo;

    /**
     * 起始日期（yyMMdd）
     */
    @ApiModelProperty("通信机日期时间（yyMMdd）")
    @TcpDownOffset(start = 9, end = 11, dateFormat = "yyMMdd")
    private Date time=new Date();

    /**
     * 终止日期（yyMMdd）
     */
    @ApiModelProperty("终止日期（yyMMdd）")
    @TcpDownOffset(start =12, end = 14, dateFormat = "yyMMdd")
    private Date time2=new Date();

    public GetSensorRainData(String sensorNo, Date time, Date time2) {
        this.sensorNo = sensorNo;
        this.time = time;
        this.time2 = time2;
    }

    public GetSensorRainData(boolean isInit, String sensorNo, Date time, Date time2) {
        super(isInit);
        this.sensorNo = sensorNo;
        this.time = time;
        this.time2 = time2;
    }
}