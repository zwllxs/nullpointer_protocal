package ltd.nullpointer.frontend.model.dto.up;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangweilin
 * @Description: 公式
 * @date 2020/4/2
 */
@Api("公式")
@Data
public class FormulaDto {

    /**
     * 传感器编号
     */
    @ApiModelProperty("传感器编号")
    @NotEmpty
    private String sensorNo;


    /**
     * 初始值
     */
    @ApiModelProperty("初始值,临时用下划线")
    private Double initial_value;

    /**
     * 水平高度
     */
    @ApiModelProperty("水平高度")
    private Double height;

    /**
     * 安装深度
     */
    @ApiModelProperty("安装深度")
    private Double depth;

    /**
     * 比例系数
     */
    @ApiModelProperty("比例系数")
    private Double ratio;

    /**
     * 计数控制
     */
    @ApiModelProperty("计数控制,临时用下划线")
    private Double control_flag;

}
