package ltd.nullpointer.frontend.model.dto.up;

import lombok.Data;
import lombok.ToString;
import ltd.nullpointer.tcp.core.annotation.DeviceSn;
import ltd.nullpointer.tcp.core.annotation.TcpUpMessage;
import ltd.nullpointer.tcp.core.annotation.TcpUpOffset;
import ltd.nullpointer.tcp.core.constant.TCPEnum;
import ltd.nullpointer.tcp.core.resolver.parser.Height4ToIntResolverParser;
import ltd.nullpointer.tcp.core.resolver.parser.Low4ToIntResolverParser;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/8
 */
@TcpUpMessage(start = 7,messageCode="557A20")
@Data
@ToString(callSuper = true)
public class SensorData1BDto {
    /**
     * 记录个数
     */
    @TcpUpOffset(start = 1, end = 2, resolveType = TCPEnum.ResolveType.hexByte2Int)
    private Integer recordNum;

    /**
     * 传感器编号
     */
    @TcpUpOffset(start = 3, end = 10)
    @DeviceSn
    private String sensorNo;

    /**
     * 传感器应变单位
     */
    @TcpUpOffset(start = 11, end = 14, resolveType = TCPEnum.ResolveType.ascii)
    private String unit;


    /**
     * 小数点位(高4位)
     */
    @TcpUpOffset(start = 15, end = 15,repeat = true, resolverParser = Height4ToIntResolverParser.class)
    private Integer num;

    /**
     * 传感器类型(低4位)
     */
    @TcpUpOffset(start = 15, end = 15,resolverParser = Low4ToIntResolverParser.class)
    private Integer type;


    /**
     * 传感器自编号
     */
    @TcpUpOffset(start = 16, end = 31, resolveType = TCPEnum.ResolveType.ascii)
    private String sensorSelfNo;

    /**
     * 传感器型号
     */
    @TcpUpOffset(start = 32, end = 39, resolveType = TCPEnum.ResolveType.ascii)
    private String model;

    /**
     * 循环记录
     */
    @TcpUpOffset(start = 40, loopClass = Data.class)
    private List<Data> dataList;

    @lombok.Data
    public static class Data {

        /**
         * 测量时间
         */
        @TcpUpOffset(start = 1, end = 6, dateFormat = "yyMMddHHmmss")
        private Date measuringDate;

        /**
         * 温度
         */
        @TcpUpOffset(start = 7, end = 8,  calcExpression = "this*0.1M",resolveType = TCPEnum.ResolveType.hexByte2UnsignedInt)
        private BigDecimal temp;

        /**
         * 测量值
         */
        @TcpUpOffset(start = 9, end = 10, calcExpression = "this/math.pow(10, sensorData1BDto_num)", resolveType = TCPEnum.ResolveType.hexByte2Int)
        private BigDecimal measurement;

        /**
         * 偏移值
         */
        @TcpUpOffset(start = 11, end = 12, calcExpression = "this/math.pow(10, sensorData1BDto_num)", resolveType = TCPEnum.ResolveType.hexByte2Int)
        private BigDecimal deviation;

        /**
         * 传感器应变频率
         */
        @TcpUpOffset(start = 13, end = 14, calcExpression = "this*0.1M", resolveType = TCPEnum.ResolveType.hexByte2Int)
        private BigDecimal strainFre;

        /**
         * 传感器补偿频率
         */
        @TcpUpOffset(start = 15, end = 16, calcExpression = "this*0.1M", resolveType = TCPEnum.ResolveType.hexByte2Int)
        private BigDecimal comFre;
    }

}
