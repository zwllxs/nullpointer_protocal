package ltd.nullpointer.frontend.model.entity.up;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import ltd.nullpointer.tcp.core.annotation.DeviceSn;
import ltd.nullpointer.tcp.core.annotation.TcpUpMessage;
import ltd.nullpointer.tcp.core.annotation.TcpUpOffset;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author zhangweilin
 * @Description: 传感器自设编号
 * @date 2019/12/7
 */
@ApiModel("传感器自设编号")
@TcpUpMessage(start = 7,messageCode="557A82")
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_sensor_set_self_no set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class SensorSetSelfNo extends LongBaseEntity {

    /**
     * 传感器编号
     */
    @ApiModelProperty("传感器编号")
    @TcpUpOffset(start = 1, end = 8)
    @DeviceSn
    private String sensorNo;

}