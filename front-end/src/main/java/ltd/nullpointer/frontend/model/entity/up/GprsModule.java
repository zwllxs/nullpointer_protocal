package ltd.nullpointer.frontend.model.entity.up;

import com.boot2.core.LongBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import ltd.nullpointer.tcp.core.annotation.TcpUpMessage;
import ltd.nullpointer.tcp.core.annotation.TcpUpOffset;
import ltd.nullpointer.tcp.core.constant.TCPEnum.ResolveType;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zhangweilin
 * @Description: xxxxx
 * @date 2019/12/7
 */
@ApiModel("GPRS模块信息")
@Data
@ToString(callSuper = true)
@Entity
@Table
@SQLDelete(sql = "update np_biz_gprs_module set is_deleted = id where id = ?")
@Where(clause = "is_deleted is false")
@DynamicInsert // 加上此注解，可以只添加有值的字段
@DynamicUpdate // 此注解，是指，会先查，然后跟参数对比，不一样的就更新，所以并不是有值更新，如果db中有值，参数里没值，那么db中的值会清空
public class GprsModule extends LongBaseEntity {

    /**
     * 模块标识
     */
    @ApiModelProperty("模块标识")
    private String identification;

    /**
     * 心跳间隔时间
     */
    @ApiModelProperty("心跳间隔时间")
    private Integer interval;

    /**
     * 传感器编号
     */
    @ApiModelProperty("传感器编号")
    private String stationNo;

    /**
     * 模块温度
     */
    @ApiModelProperty("模块温度")
    private BigDecimal temp;

    /**
     * 模块电压
     */
    @ApiModelProperty("模块电压")
    private BigDecimal voltage;

    /**
     * 绑定的传感器
     */
    @ApiModelProperty("绑定的设备")
    private String  sensorNo;

}