package ltd.nullpointer.frontend.dao;

import com.boot2.core.dao.jpa.AbsctractDao;
import ltd.nullpointer.frontend.model.entity.SensorRainDataDetail;

import java.util.Date;

//@DataSource("test2")
public interface SensorRainDataDetailDao extends AbsctractDao<SensorRainDataDetail> {


    /**
     * 用于去重
     * @param sensorNo
     * @param measuringDate
     * @return
     */
    boolean existsBySensorNoAndMeasuringDate(String sensorNo, Date measuringDate);
}