package ltd.nullpointer.frontend.dao;

import com.boot2.core.dao.jpa.AbsctractDao;
import ltd.nullpointer.frontend.model.entity.down.GetSensorData;

//@DataSource("test2")
public interface GetSensorDataDao extends AbsctractDao<GetSensorData> {

}