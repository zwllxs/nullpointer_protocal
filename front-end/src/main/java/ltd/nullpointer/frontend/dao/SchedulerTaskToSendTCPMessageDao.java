package ltd.nullpointer.frontend.dao;

import com.boot2.core.dao.jpa.AbsctractDao;
import ltd.nullpointer.frontend.model.entity.SchedulerTaskToSendTCPMessage;

import java.util.List;

//@DataSource("test2")
public interface SchedulerTaskToSendTCPMessageDao extends AbsctractDao<SchedulerTaskToSendTCPMessage> {

    /**
     * 按传感器编号查
      * @param sensorNo
     * @return
     */
    List<SchedulerTaskToSendTCPMessage> findBySensorNo(String sensorNo);

}