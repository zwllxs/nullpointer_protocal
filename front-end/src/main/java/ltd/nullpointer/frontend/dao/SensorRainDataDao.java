package ltd.nullpointer.frontend.dao;

import com.boot2.core.dao.jpa.AbsctractDao;
import ltd.nullpointer.frontend.model.entity.SensorRainData;

//@DataSource("test2")
public interface SensorRainDataDao extends AbsctractDao<SensorRainData> {

    /**
     * 查一下
     * @param sensorNo
     * @return
     */
    SensorRainData findBySensorNo(String sensorNo);
}