package ltd.nullpointer.frontend.dao;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

/**
 * @author zhangweilin
 * @Description: 加统一前缀
 * @date 2019/12/16
 */
//public class NpBizNamingStrategy extends PhysicalNamingStrategyStandardImpl {
public class NpBizNamingStrategy extends SpringPhysicalNamingStrategy {

    private final String databasePrefix="np_biz_";

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        Identifier identifier = Identifier.toIdentifier(databasePrefix + super.toPhysicalTableName(name, context));
        return identifier;
    }
}
