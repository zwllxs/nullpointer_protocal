package ltd.nullpointer.frontend.dao;

import com.boot2.core.dao.jpa.AbsctractDao;
import ltd.nullpointer.frontend.model.entity.up.GprsModule;

import java.util.Date;

//@DataSource("test2")
public interface GprsModuleDao extends AbsctractDao<GprsModule> {

    /**
     * 用于去重
     * @param stationNo
     * @return
     */
    boolean existsByStationNo(String stationNo);

    /**
     * 用于去重
     * @param stationNo
     * @param sensorNo
     * @return
     */
    boolean existsByStationNoAndSensorNo(String stationNo, String sensorNo);

}