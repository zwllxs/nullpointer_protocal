package ltd.nullpointer.frontend.facade;

import ltd.nullpointer.frontend.model.entity.down.*;
import ltd.nullpointer.tcp.core.TCPMessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author zhangweilin
 * @Description: 专门处理上报的消息实体
 * @date 2019/12/19
 */
@Component
public class SensorMessageDownFacade {

    @Autowired
    TCPMessageProducer tcpMessageProducer;

    /**
     * 打开自动采集箱总线电源 ,命令字 6CH
     *
     * @param sensorNo
     */
    @Async
    public void handTurnOn(String sensorNo) {
        TurnOn turnOn = new TurnOn(true);
        tcpMessageProducer.sendToDevice(sensorNo, turnOn);
    }

    /**
     * 采集传感器编号,命令字 01H
     *
     * @param sensorNo
     * @param time
     */
    @Async
    public void handGetSensorNo(String sensorNo, Date time) {
        GetSensorNo getSensorNo = new GetSensorNo(true, sensorNo, time);
        tcpMessageProducer.sendToDevice(sensorNo, getSensorNo);
    }

    /**
     * 采集传感器数据,命令字 10H
     *
     * @param sensorNo
     * @param time
     */
    @Async
    public void handGetSensorData(String sensorNo, Date time) {
        GetSensorData getSensorData = new GetSensorData(true, sensorNo, time);
        tcpMessageProducer.sendToDevice(sensorNo, getSensorData);
    }

    /**
     * 清除传感器中保存的数据记录,命令字 21H
     *
     * @param sensorNo
     * @param time
     */
    @Async
    public void handClearSensorDataLog(String sensorNo, Date time) {
        ClearSensorDataLog getSensorData = new ClearSensorDataLog(true, sensorNo, time);
        tcpMessageProducer.sendToDevice(sensorNo, getSensorData);
    }


    /**
     * 采集传感器中保存的数据记录,命令字 20H
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @Async
    public void handGetSensorDataLog(String sensorNo, Date time) {
        GetSensorDataLog getSensorDataLog = new GetSensorDataLog(true, sensorNo, time);
        tcpMessageProducer.sendToDevice(sensorNo, getSensorDataLog);
    }

    /**
     * 传感器调零,命令字 80H
     *
     * @param sensorNo
     * @param time
     * @return
     */
    @Async
    public void handSetSensorZero(String sensorNo, Date time) {
        SetSensorZero setSensorZero = new SetSensorZero(true, sensorNo, time);
        tcpMessageProducer.sendToDevice(sensorNo, setSensorZero);
    }

    /**
     * 读取指定日期雨量数据,命令字 1BH
     *
     * @param sensorNo
     * @param time1
     * @param time2
     * @return
     */
    @Async
    public void handGetSensorRainData(String sensorNo, Date time1, Date time2) {
        GetSensorRainData getSensorRainData = new GetSensorRainData(sensorNo, time1, time2);
        tcpMessageProducer.sendToDevice(sensorNo, getSensorRainData);
    }

    /**
     * 设置传感器自设编号,命令字 82H
     *
     * @param sensorNo
     * @param selfNo
     * @param time
     * @return
     */
    @Async
    public void handSetSensorSelfNo(String sensorNo, String selfNo, Date time) {
        SetSensorSelfNo setSensorSelfNo = new SetSensorSelfNo(sensorNo, time, selfNo);
        tcpMessageProducer.sendToDevice(sensorNo, setSensorSelfNo);
    }

    /**
     * 采集传感器数据并保存至传感器 命令字 11H
     *
     * @param sensorNo
     * @param time
     */
    @Async
    public void handGetSensorDataAndStore(String sensorNo, Date time) {
        GetSensorDataAndStore getSensorDataAndStore = new GetSensorDataAndStore(sensorNo, time);
        tcpMessageProducer.sendToDevice(sensorNo, getSensorDataAndStore);
    }


}
